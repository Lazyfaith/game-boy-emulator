# README #

This is a Game Boy emulator written in Java that I completed in my third and final year at university. 

# Goals, Features & Limitations #

The project goals were to make an emulator that was accurate enough to play a chosen game well (the chosen game for this project was the international release version of Tetris). 

Whilst the project does not have many "additional" features outside of the virtual machine that is the core of the program; it does have support for emulator save states, pausing/resuming program execution, keybind changings and changing of screen settings.

The limitations of the emulator are that there's no sound, no multiplayer (link cable) emulation support and it is most likely only accurate enough to play Tetris.

# Tools / Resources #

The main development resource for this project was the [Game Boy CPU Manual](http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf) which is highly recommended for anyone developing a Game Boy emulator. Further resources include Stack Overflow and 20 year old posts on emulator forums (which were an interesting read).

The software was written in IntelliJ and does not make use of any third party libraries.