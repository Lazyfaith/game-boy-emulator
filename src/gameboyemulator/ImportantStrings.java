package gameboyemulator;

public class ImportantStrings {
    public static final String MAINWINDOWTITLE = "Game Boy Emulator";
    public static final String SETTINGSWINDOWTITLE = MAINWINDOWTITLE + " Settings";
    public static final String ERRORWINDOWTITLE = MAINWINDOWTITLE + " - Error";
    public static final String HELPWINDOWTITLE = MAINWINDOWTITLE + " - Help";

    public static final String LOADROM = "Load ROM...";
    public static final String LOADSTATE = "Load State...";
    public static final String SAVESTATE = "Save State...";
    public static final String EXIT = "Exit";

    public static final String START = "Start / Resume";
    public static final String PAUSESTEP = "Pause / Step";
    public static final String HARDRESET = "Hard Reset";

    public static final String SETTINGS = "Settings...";
}
