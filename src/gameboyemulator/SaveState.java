package gameboyemulator;

import java.io.*;

public class SaveState {

    /*
        These are all the values that the emulator uses and are important to keep consistency between state saves/loads.

        A Game Boy saved state file will just be these values written as String to a file, each value on a new line
            and the values are written to the file in the order that they appear below.

        It is important to read/write the values in the same order for states to save/load correctly.
     */

    // Register data
    public int registerA;
    public int registerF;
    public int registerB;
    public int registerC;
    public int registerD;
    public int registerE;
    public int registerH;
    public int registerL;

    public boolean IME;
    public int SP;
    public int PC;

    // Emulation related variables
    public boolean romLoaded;
    public long cycleCountLastTimersUpdate;
    public long divCyclesSinceLastInc;
    public long timaCyclesSinceLastInc;
    public long stepCount;
    public long cpuCyclesPassed;
    public boolean imeChangeDelayed;
    public boolean activeIME;
    public boolean cpuHalted;
    public boolean cpuAndLcdStopped;
    public boolean failedHalt;

    // RAM data
    public int[] ram;

    public SaveState() {

    }

    public SaveState(File savedStateFile) throws IOException {
        // With a file in the constructor this means that we're reading the saved state from that file
        BufferedReader is = new BufferedReader(new FileReader(savedStateFile));

        // Register values
        registerA = Integer.parseInt(is.readLine());
        registerF = Integer.parseInt(is.readLine());
        registerB = Integer.parseInt(is.readLine());
        registerC = Integer.parseInt(is.readLine());
        registerD = Integer.parseInt(is.readLine());
        registerE = Integer.parseInt(is.readLine());
        registerH = Integer.parseInt(is.readLine());
        registerL = Integer.parseInt(is.readLine());

        IME = Boolean.parseBoolean(is.readLine());

        SP = Integer.parseInt(is.readLine());
        PC = Integer.parseInt(is.readLine());

        // Emulation related variables
        cycleCountLastTimersUpdate = Long.parseLong(is.readLine());
        divCyclesSinceLastInc = Long.parseLong(is.readLine());
        timaCyclesSinceLastInc = Long.parseLong(is.readLine());
        stepCount = Long.parseLong(is.readLine());
        cpuCyclesPassed = Long.parseLong(is.readLine());

        imeChangeDelayed = Boolean.parseBoolean(is.readLine());
        activeIME = Boolean.parseBoolean(is.readLine());
        cpuHalted = Boolean.parseBoolean(is.readLine());
        cpuAndLcdStopped = Boolean.parseBoolean(is.readLine());
        failedHalt = Boolean.parseBoolean(is.readLine());

        // All values in RAM
        ram = new int[0xFFFF+1];
        for (int i=0; i<ram.length; i++) {
            ram[i] = Integer.parseInt(is.readLine());
        }
    }

    public void saveStateToFile(File fileToSaveTo) throws IOException {
        // If the file already exists, then delete it
        if (fileToSaveTo.exists())
            fileToSaveTo.delete();

        // Now start the process of writing all the values to the file
        fileToSaveTo.createNewFile();
        BufferedWriter os = new BufferedWriter(new FileWriter(fileToSaveTo));

        // Register values
        os.write(Integer.toString(registerA)); os.newLine();
        os.write(Integer.toString(registerF)); os.newLine();
        os.write(Integer.toString(registerB)); os.newLine();
        os.write(Integer.toString(registerC)); os.newLine();
        os.write(Integer.toString(registerD)); os.newLine();
        os.write(Integer.toString(registerE)); os.newLine();
        os.write(Integer.toString(registerH)); os.newLine();
        os.write(Integer.toString(registerL)); os.newLine();

        os.write(Boolean.toString(IME)); os.newLine();

        os.write(Integer.toString(SP)); os.newLine();
        os.write(Integer.toString(PC)); os.newLine();

        // Emulation related variables
        os.write(Long.toString(cycleCountLastTimersUpdate)); os.newLine();
        os.write(Long.toString(divCyclesSinceLastInc)); os.newLine();
        os.write(Long.toString(timaCyclesSinceLastInc)); os.newLine();
        os.write(Long.toString(stepCount)); os.newLine();
        os.write(Long.toString(cpuCyclesPassed)); os.newLine();

        os.write(Boolean.toString(imeChangeDelayed)); os.newLine();
        os.write(Boolean.toString(activeIME)); os.newLine();
        os.write(Boolean.toString(cpuHalted)); os.newLine();
        os.write(Boolean.toString(cpuAndLcdStopped)); os.newLine();
        os.write(Boolean.toString(failedHalt)); os.newLine();

        // All values in RAM
        for (int i=0; i<ram.length; i++){
            os.write(Integer.toString(ram[i]));
            os.newLine();
        }

        os.flush();
        os.close();
    }
}
