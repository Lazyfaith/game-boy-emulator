package gameboyemulator;

import gameboyemulator.Settings.SettingsWindow;
import gameboyemulator.Settings.UserSettings;
import gameboyemulator.virtualmachine.GameBoyCore;
import gameboyemulator.virtualmachine.Screen;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

public class Main extends JFrame implements ActionListener, Thread.UncaughtExceptionHandler {

    static GameBoyCore vm;
    static Thread emulationThread;
    static SettingsWindow settingsWindow;

    public static void main(String[] args){
        Main mainWindow = new Main();
    }

    public Main() {
        super(ImportantStrings.MAINWINDOWTITLE);

        // Before doing anything, get the current user settings & enforce them
        UserSettings currentSettings = new UserSettings();
        try {
            currentSettings.loadUserSettings();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "ERROR whilst reading from settings file.\n\n" + e.getMessage() + "\n\nDefault settings are being used.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();

            currentSettings = new UserSettings();
        }
        currentSettings.enforceUserSettings();

        vm = new GameBoyCore();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(vm.screen);
        this.setSize(Screen.SCREENWIDTH * Screen.SCALAR, Screen.SCREENHEIGHT * Screen.SCALAR);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.addKeyListener(vm.deviceInput);

        // Menubar at top of window
        JMenuBar menuBar = new JMenuBar();
        JMenuItem menuItem = null;

        // File menu
        JMenu fileMenu = new JMenu("File");

        menuItem = new JMenuItem(ImportantStrings.LOADROM);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        fileMenu.addSeparator();

        menuItem = new JMenuItem(ImportantStrings.LOADSTATE);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        menuItem = new JMenuItem(ImportantStrings.SAVESTATE);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        fileMenu.addSeparator();

        menuItem = new JMenuItem(ImportantStrings.EXIT);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        // Emulation menu
        JMenu emulationMenu = new JMenu("Emulation");

        menuItem = new JMenuItem(ImportantStrings.START);
        menuItem.addActionListener(this);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        emulationMenu.add(menuItem);

        menuItem = new JMenuItem(ImportantStrings.PAUSESTEP);
        menuItem.addActionListener(this);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
        emulationMenu.add(menuItem);

        emulationMenu.addSeparator();

        menuItem = new JMenuItem(ImportantStrings.HARDRESET);
        menuItem.addActionListener(this);
        emulationMenu.add(menuItem);

        // Window menu
        JMenu windowMenu = new JMenu("Window");

        menuItem = new JMenuItem(ImportantStrings.SETTINGS);
        menuItem.addActionListener(this);
        windowMenu.add(menuItem);

        // Assemble the menu bar and add to JFrame
        menuBar.add(fileMenu);
        menuBar.add(emulationMenu);
        menuBar.add(windowMenu);
        this.setJMenuBar(menuBar);

        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = ((JMenuItem) e.getSource()).getText();

        switch(action) {
            case ImportantStrings.LOADROM:
                menuLoadRoam();
                break;
            case ImportantStrings.LOADSTATE:
                menuLoadState();
                break;
            case ImportantStrings.SAVESTATE:
                menuSaveState();
                break;
            case ImportantStrings.EXIT:
                System.exit(0);
                break;
            case ImportantStrings.START:
                menuStartResumeEmulator();
                break;
            case ImportantStrings.PAUSESTEP:
                menuPauseStepEmulator();
                break;
            case ImportantStrings.HARDRESET:
                menuHardResetEmulator();
                break;
            case ImportantStrings.SETTINGS:
                menuSettings();
                break;
        }
    }

    private void menuLoadRoam() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Game Boy ROMs (.gb, .rom, .bin)", "gb", "rom", "bin"));
        int result = fileChooser.showOpenDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {
            File rom = fileChooser.getSelectedFile();

            if (!rom.canRead()) {
                JOptionPane.showMessageDialog(null, "File could not be read.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (rom.length() > (32 * 1024)) {
                JOptionPane.showMessageDialog(null, "File is too large to be a supported ROM, maximum ROM size supported is 32KB. This ROM may run inaccurately or cause the emulator to crash.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
            }

            menuHardResetEmulator();

            try {
                vm.loadRom(rom);
            }
            catch (IOException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error whilst reading ROM file:\n\n" + e.getMessage(), ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
                return;
            }

            this.setTitle(ImportantStrings.MAINWINDOWTITLE + " - " + rom.getName());
            JOptionPane.showMessageDialog(null, "ROM \"" + rom.getName() + "\" loaded into emulator.", ImportantStrings.MAINWINDOWTITLE, JOptionPane.INFORMATION_MESSAGE);
            menuStartResumeEmulator();
        }
    }

    private void menuLoadState() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Game Boy Saved State (.gbss)", "gbss"));
        int result = fileChooser.showOpenDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {
            File savedStateFile = fileChooser.getSelectedFile();

            if (!savedStateFile.canRead()) {
                JOptionPane.showMessageDialog(null, "File could not be read.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
                return;
            }

            SaveState savedState = null;
            try {
                savedState = new SaveState(savedStateFile);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "There was an error when attempting to read the save state file.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }

            menuHardResetEmulator();
            vm.loadState(savedState);
        }
    }

    private void menuSaveState() {
        // Make sure a game has actually been loaded before doing this
        if (!vm.isRomLoaded()) {
            JOptionPane.showMessageDialog(null, "You must have a game loaded and running before you can save a state!", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Make sure emulator isn't still running before doing this
        if (vm.isRunning()) {
            vm.stop();
        }

        // Ask user where they want this to be saved to
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Game Boy Saved State (.gbss)", "gbss"));
        int result = fileChooser.showSaveDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {
            File fileToSaveTo = fileChooser.getSelectedFile();

            // Get the save state information from the VM
            SaveState state = vm.generateSaveState();

            try {
                state.saveStateToFile(fileToSaveTo);
                System.out.println("Save state file finished.");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "There was an error whilst attempting to write the save state file.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }
    }

    private void menuStartResumeEmulator() {
        if (!vm.isRunning()) {
            startVM();
        }
    }

    private void startVM() {
        if (emulationThread != null && emulationThread.isAlive())
            emulationThread.interrupt();

        emulationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                vm.start();
            }
        });
        emulationThread.start();
        emulationThread.setUncaughtExceptionHandler(this);
    }

    private void menuPauseStepEmulator() {
        if (!vm.isRomLoaded())
            return;

        if (vm.isRunning())
            vm.stop();
        else
            vm.singleTick();
    }

    private void menuHardResetEmulator() {
        if (vm.isRunning())
            vm.stop();

        if (emulationThread != null && emulationThread.isAlive())
            emulationThread.interrupt();

        vm.hardReset();
        this.setTitle(ImportantStrings.MAINWINDOWTITLE);

        // Old deviceInput was lost during reset, so need to assign new one as key listener
        this.addKeyListener(vm.deviceInput);
    }

    private void menuSettings() {
        // If the VM is running then stop it before opening settings window
        if (vm.isRunning())
            vm.stop();

        // Ensure an instance of the settings window exists
        if (settingsWindow == null) {
            settingsWindow = new SettingsWindow();
        }

        // Display the settings window and bring it to the front
        if (!settingsWindow.isVisible())
            settingsWindow.setVisible(true);

        settingsWindow.toFront();
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("UNCAUGHT EXCEPTION");
        e.printStackTrace();
        JOptionPane.showMessageDialog(null, "Emulation has stopped due to an uncaught error whilst running the ROM.\n\n" + e.getMessage(), ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
    }
}