package gameboyemulator.Settings;

import gameboyemulator.virtualmachine.DeviceInput;
import gameboyemulator.virtualmachine.Screen;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;

public class UserSettings {

    private static File settingsFile = new File("UserSettings.dat");

    /*
        Every setting is saved in the form of an integer (more accurately, it's ASCII representation), with each variable
            being on it's own line.
        This is perfectly fine for the screen scalar value and for the keybind flags as those are all integers anyway,
            but for the colours those are converted to integers by the Color class's method .getRGB() before being written
            to the file. They are then cast back to a Color simply by being put into the Color constructor.

        The values are written/read to/from the file in the order that they appear below as class variables.
     */

    // User settings with default values
    public int screenScalar = 3;

    public Color screenColourLightest = Color.WHITE;
    public Color screenColourLight = Color.LIGHT_GRAY;
    public Color screenColourDark = Color.DARK_GRAY;
    public Color screenColourDarkest = Color.BLACK;

    public int KEYBIND_DOWN = KeyEvent.VK_DOWN;
    public int KEYBIND_UP = KeyEvent.VK_UP;
    public int KEYBIND_LEFT = KeyEvent.VK_LEFT;
    public int KEYBIND_RIGHT = KeyEvent.VK_RIGHT;
    public int KEYBIND_START = KeyEvent.VK_SPACE;
    public int KEYBIND_SELECT = KeyEvent.VK_ENTER;
    public int KEYBIND_B = KeyEvent.VK_B;
    public int KEYBIND_A = KeyEvent.VK_A;

    public void loadUserSettings() throws IOException {
        if (!settingsFile.exists()) {
            // Settings file does not exist, so keep using default values
            return;
        }

        // Settings file exists, so read the values from it
       BufferedReader is = new BufferedReader(new FileReader(settingsFile));

        // Screen scalar
        screenScalar = Integer.parseInt(is.readLine());
        // Screen colours values
        screenColourLightest = new Color(Integer.parseInt(is.readLine()));
        screenColourLight = new Color(Integer.parseInt(is.readLine()));
        screenColourDark = new Color(Integer.parseInt(is.readLine()));
        screenColourDarkest = new Color(Integer.parseInt(is.readLine()));
        // Keybind values
        KEYBIND_DOWN = Integer.parseInt(is.readLine());
        KEYBIND_UP = Integer.parseInt(is.readLine());
        KEYBIND_LEFT = Integer.parseInt(is.readLine());
        KEYBIND_RIGHT = Integer.parseInt(is.readLine());
        KEYBIND_START = Integer.parseInt(is.readLine());
        KEYBIND_SELECT = Integer.parseInt(is.readLine());
        KEYBIND_B = Integer.parseInt(is.readLine());
        KEYBIND_A = Integer.parseInt(is.readLine());

        is.close();
    }

    public void saveAndEnforceUserSettings() throws IOException {
        // Save the new user settings to the settings file
        saveUserSettings();
        // Then update the appropriate values from various classes to match these settings
        enforceUserSettings();
    }

    private void saveUserSettings() throws IOException {
        // Delete any existing settings file
        if (settingsFile.exists())
            settingsFile.delete();

        // Now write the new settings file with the latest settings
        settingsFile.createNewFile();
        BufferedWriter os = new BufferedWriter(new FileWriter(settingsFile));

        // Screen scalar
        os.write(Integer.toString(screenScalar));
        os.newLine();
        // Screen colours values
        os.write(Integer.toString(screenColourLightest.getRGB()));
        os.newLine();
        os.write(Integer.toString(screenColourLight.getRGB()));
        os.newLine();
        os.write(Integer.toString(screenColourDark.getRGB()));
        os.newLine();
        os.write(Integer.toString(screenColourDarkest.getRGB()));
        os.newLine();
        // Keybind values
        os.write(Integer.toString(KEYBIND_DOWN));
        os.newLine();
        os.write(Integer.toString(KEYBIND_UP));
        os.newLine();
        os.write(Integer.toString(KEYBIND_LEFT));
        os.newLine();
        os.write(Integer.toString(KEYBIND_RIGHT));
        os.newLine();
        os.write(Integer.toString(KEYBIND_START));
        os.newLine();
        os.write(Integer.toString(KEYBIND_SELECT));
        os.newLine();
        os.write(Integer.toString(KEYBIND_B));
        os.newLine();
        os.write(Integer.toString(KEYBIND_A));

        os.flush();
        os.close();
    }

    public void enforceUserSettings() {
        Screen.SCALAR = screenScalar;
        Screen.SCREENCOLOURLIGHTEST = screenColourLightest;
        Screen.SCREENCOLOURLIGHT = screenColourLight;
        Screen.SCREENCOLOURDARK = screenColourDark;
        Screen.SCREENCOLOURDARKEST = screenColourDarkest;

        DeviceInput.KEYBIND_DOWN = KEYBIND_DOWN;
        DeviceInput.KEYBIND_UP = KEYBIND_UP;
        DeviceInput.KEYBIND_LEFT = KEYBIND_LEFT;
        DeviceInput.KEYBIND_RIGHT = KEYBIND_RIGHT;
        DeviceInput.KEYBIND_START = KEYBIND_START;
        DeviceInput.KEYBIND_SELECT = KEYBIND_SELECT;
        DeviceInput.KEYBIND_B = KEYBIND_B;
        DeviceInput.KEYBIND_A = KEYBIND_A;
    }
}
