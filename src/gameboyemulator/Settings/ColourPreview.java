package gameboyemulator.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

class ColourPreview extends JPanel {
    ColourPreview(Color colourToDisplay, MouseListener mouseListener) {
        super();
        this.setBackground(colourToDisplay);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setPreferredSize(new Dimension(50, 50));
        this.addMouseListener(mouseListener);
    }

    public void updatePreview(Color colourToDisplay) {
        this.setBackground(colourToDisplay);
    }
}
