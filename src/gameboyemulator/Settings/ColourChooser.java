package gameboyemulator.Settings;

import javax.swing.*;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import java.awt.*;

public class ColourChooser extends JColorChooser {

    JPanel colourPreviewPanel;

    public ColourChooser(Color starterColour) {
        super();

        // Hide all colour selection panels except for RGB selection
        AbstractColorChooserPanel[] colorChooserPanels = this.getChooserPanels();
        for(AbstractColorChooserPanel panel : colorChooserPanels) {
            if (!panel.getDisplayName().equals("RGB"))
                this.removeChooserPanel(panel);
        }

        // Remove the built in preview bar
       this.setPreviewPanel(new JPanel());
    }
}
