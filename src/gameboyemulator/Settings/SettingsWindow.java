package gameboyemulator.Settings;

import gameboyemulator.ImportantStrings;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class SettingsWindow extends JFrame implements MouseListener, ActionListener, ChangeListener, KeyListener {

    UserSettings currentSettings;

    ColourChooser colourChooserDialog;
    int lastColourClicked;

    JSpinner scalarSpinner;
    SpinnerModel spinnerModel;

    ColourPreview colour0;
    ColourPreview colour1;
    ColourPreview colour2;
    ColourPreview colour3;

    JTextField txtButtonDown;
    JTextField txtButtonUp;
    JTextField txtButtonLeft;
    JTextField txtButtonRight;
    JTextField txtButtonStart;
    JTextField txtButtonSelect;
    JTextField txtButtonB;
    JTextField txtButtonA;

    JButton btnSaveSettings;
    JButton btnResetSettings;


    public SettingsWindow() {
        super(ImportantStrings.SETTINGSWINDOWTITLE);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setMinimumSize(new Dimension(425, 425));
        this.setLocationRelativeTo(null);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));

        // Load the current user settings from the file
        currentSettings = new UserSettings();
        try {
            currentSettings.loadUserSettings();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "ERROR whilst reading from settings file.\n\n" + e.getMessage() + "\n\nDefault settings are being used.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();

            currentSettings = new UserSettings();
        }

        // Build different sections of the UI

        // Screen settings sections
        JPanel colourSelectionSection = new JPanel();
        colourSelectionSection.setBorder(BorderFactory.createTitledBorder("Display Settings"));

        // Scalar
        JLabel lblScalar = new JLabel("Screen size multiplier");
        JLabel lblScalarNotice = new JLabel("<html>Note: For changes to screen size multiplier to take affect, completely close & restart the emulator.<br><br><html>");
        lblScalarNotice.setFont(new Font(lblScalarNotice.getFont().getName(), Font.ITALIC, lblScalarNotice.getFont().getSize())); // Adjust font to be italic
        spinnerModel = new SpinnerNumberModel(currentSettings.screenScalar, 1, 20, 1);
        spinnerModel.addChangeListener(this);
        scalarSpinner = new JSpinner(spinnerModel);
        scalarSpinner.setMaximumSize(new Dimension(25, 25));

        // Screen colours
        JLabel lblColourExplanation = new JLabel("<html>The real Game Boy display shows 4 different shades; white, light grey, dark grey and black.<br>Click on the shade below that you wish to change the colour of.</html>");
        JLabel lblColour0 = new JLabel("Colour 0 (White)");
        JLabel lblColour1 = new JLabel("Colour 1 (Light)");
        JLabel lblColour2 = new JLabel("Colour 2 (Dark)");
        JLabel lblColour3 = new JLabel("Colour 3 (Black)");

        colour0 = new ColourPreview(currentSettings.screenColourLightest, this);
        colour1 = new ColourPreview(currentSettings.screenColourLight, this);
        colour2 = new ColourPreview(currentSettings.screenColourDark, this);
        colour3 = new ColourPreview(currentSettings.screenColourDarkest, this);

        // Now assemble the screen settings layout
        GroupLayout groupLayout = new GroupLayout(colourSelectionSection);
        colourSelectionSection.setLayout(groupLayout);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup()
                        .addGroup(groupLayout.createSequentialGroup()
                                        .addComponent(lblScalar)
                                        .addComponent(scalarSpinner)
                        )
                        .addComponent(lblScalarNotice)
                        .addComponent(lblColourExplanation)
                        .addGroup(groupLayout.createSequentialGroup()
                                        .addGroup(groupLayout.createParallelGroup()
                                                        .addComponent(lblColour0)
                                                        .addComponent(colour0)
                                        )
                                        .addGroup(groupLayout.createParallelGroup()
                                                        .addComponent(lblColour1)
                                                        .addComponent(colour1)
                                        )
                                        .addGroup(groupLayout.createParallelGroup()
                                                        .addComponent(lblColour2)
                                                        .addComponent(colour2)
                                        )
                                        .addGroup(groupLayout.createParallelGroup()
                                                        .addComponent(lblColour3)
                                                        .addComponent(colour3)
                                        )
                        )
        );

        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addGroup(groupLayout.createParallelGroup()
                                        .addComponent(lblScalar)
                                        .addComponent(scalarSpinner)
                        )
                        .addComponent(lblScalarNotice)
                        .addComponent(lblColourExplanation)
                        .addGroup(groupLayout.createParallelGroup()
                                        .addComponent(lblColour0)
                                        .addComponent(lblColour1)
                                        .addComponent(lblColour2)
                                        .addComponent(lblColour3)
                        )
                        .addGroup(groupLayout.createParallelGroup()
                                        .addComponent(colour0)
                                        .addComponent(colour1)
                                        .addComponent(colour2)
                                        .addComponent(colour3)
                        )
        );

        // Keybinds section
        JPanel keybindsSection = new JPanel();
        keybindsSection.setBorder(BorderFactory.createTitledBorder("Keybinds"));

        txtButtonDown = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_DOWN));
        txtButtonDown.addKeyListener(this);
        txtButtonUp = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_UP));
        txtButtonUp.addKeyListener(this);
        txtButtonLeft = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_LEFT));
        txtButtonLeft.addKeyListener(this);
        txtButtonRight = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_RIGHT));
        txtButtonRight.addKeyListener(this);
        txtButtonStart = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_START));
        txtButtonStart.addKeyListener(this);
        txtButtonSelect = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_SELECT));
        txtButtonSelect.addKeyListener(this);
        txtButtonB = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_B));
        txtButtonB.addKeyListener(this);
        txtButtonA = new JTextField(KeyEvent.getKeyText(currentSettings.KEYBIND_A));
        txtButtonA.addKeyListener(this);

        keybindsSection.setLayout(new GridLayout(4, 4));
        keybindsSection.add(new JLabel("Down"));
        keybindsSection.add(txtButtonDown);
        keybindsSection.add(new JLabel("Up"));
        keybindsSection.add(txtButtonUp);
        keybindsSection.add(new JLabel("Left"));
        keybindsSection.add(txtButtonLeft);
        keybindsSection.add(new JLabel("Right"));
        keybindsSection.add(txtButtonRight);
        keybindsSection.add(new JLabel("Start"));
        keybindsSection.add(txtButtonStart);
        keybindsSection.add(new JLabel("Select"));
        keybindsSection.add(txtButtonSelect);
        keybindsSection.add(new JLabel("B"));
        keybindsSection.add(txtButtonB);
        keybindsSection.add(new JLabel("A"));
        keybindsSection.add(txtButtonA);

        // Lower buttons section
        JPanel lowerButtons = new JPanel();
        btnSaveSettings = new JButton("Save & Apply Settings");
        btnSaveSettings.addActionListener(e -> saveAndApplyNewSettings());
        lowerButtons.add(btnSaveSettings);
        btnResetSettings = new JButton("Reset All Settings");
        btnResetSettings.addActionListener(e -> resetSettingsToDefault());
        lowerButtons.add(btnResetSettings);


        // Assemble all the pieces to display the final UI
        this.add(colourSelectionSection);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(keybindsSection);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(lowerButtons);
        this.pack();
    }

    void saveAndApplyNewSettings() {
        // Save the settings and apply
        try {
            currentSettings.saveAndEnforceUserSettings();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "ERROR whilst writing new save file.\n\n" + e.getMessage() + "\n\nAny settings changes may be lost when the program is closed.", ImportantStrings.ERRORWINDOWTITLE, JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        // Now close the settings window
        this.setVisible(false);
    }

    void resetSettingsToDefault() {
        currentSettings = new UserSettings();
        try {
            currentSettings.saveAndEnforceUserSettings();
        } catch (IOException e) {
            // Error writing to new save file, but since they would contain default values anyway it's not important
            e.printStackTrace();
        }
        updateSettingsInUI();
    }

    void updateSettingsInUI() {
        // The latest scalar value
        spinnerModel.setValue(currentSettings.screenScalar);

        // The latest colour selections
        colour0.updatePreview(currentSettings.screenColourLightest);
        colour1.updatePreview(currentSettings.screenColourLight);
        colour2.updatePreview(currentSettings.screenColourDark);
        colour3.updatePreview(currentSettings.screenColourDarkest);

        // The latest keybinds
        txtButtonDown.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_DOWN));
        txtButtonUp.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_UP));
        txtButtonLeft.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_LEFT));
        txtButtonRight.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_RIGHT));
        txtButtonStart.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_START));
        txtButtonSelect.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_SELECT));
        txtButtonB.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_B));
        txtButtonA.setText(KeyEvent.getKeyText(currentSettings.KEYBIND_A));

        this.invalidate();
    }

    // Various methods from listener interfaces

    @Override
    public void mouseClicked(MouseEvent e) {
        // Only using mouseClicked event for colour preview panels so we already know what it's for
        // Get colour currently displayed in preview & which GB colour it is
        JPanel colorPreviewClicked = (JPanel) e.getSource();

        Color currentColour = colorPreviewClicked.getBackground();

        if (colorPreviewClicked.equals(colour0))
            lastColourClicked = 0;
        else if (colorPreviewClicked.equals(colour1))
            lastColourClicked = 1;
        else if (colorPreviewClicked.equals(colour2))
            lastColourClicked = 2;
        else
            lastColourClicked = 3;

        // Display a colour picker to user
        colourChooserDialog = new ColourChooser(currentColour);
        JColorChooser.createDialog(null, ImportantStrings.MAINWINDOWTITLE + " - Colour Picker", true, colourChooserDialog, this, null).setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //  User clicked "OK" in colour chooser
        // Get the colour
        Color chosenColour = colourChooserDialog.getColor();
        // Create new colour from that with max alpha in case user changed it in selection panel
        // Doing this because we only want opaque colours
        Color finalColour = new Color(chosenColour.getRed(), chosenColour.getGreen(), chosenColour.getBlue(), 255);

        // Update it in the settings stored (but not saved) and in the display
        switch(lastColourClicked) {
            case 0:
                currentSettings.screenColourLightest = finalColour;
                colour0.setBackground(finalColour);
                break;
            case 1:
                currentSettings.screenColourLight = finalColour;
                colour1.setBackground(finalColour);
                break;
            case 2:
                currentSettings.screenColourDark = finalColour;
                colour2.setBackground(finalColour);
                break;
            case 3:
                currentSettings.screenColourDarkest = finalColour;
                colour3.setBackground(finalColour);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // User entered new keybind
        // First, get the key they pressed
        int newKeybind = e.getKeyCode();

        // Then work out which keybind it was for and update the appropriate setting
        JTextField typedField = (JTextField) e.getSource();
        if (typedField.equals(txtButtonA))
            currentSettings.KEYBIND_A = newKeybind;
        else if (typedField.equals(txtButtonB))
            currentSettings.KEYBIND_B = newKeybind;
        else if (typedField.equals(txtButtonDown))
            currentSettings.KEYBIND_DOWN = newKeybind;
        else if (typedField.equals(txtButtonUp))
            currentSettings.KEYBIND_UP = newKeybind;
        else if (typedField.equals(txtButtonLeft))
            currentSettings.KEYBIND_LEFT = newKeybind;
        else if (typedField.equals(txtButtonRight))
            currentSettings.KEYBIND_RIGHT = newKeybind;
        else if (typedField.equals(txtButtonSelect))
            currentSettings.KEYBIND_SELECT = newKeybind;
        else if (typedField.equals(txtButtonStart))
            currentSettings.KEYBIND_START = newKeybind;

        // Update UI
        updateSettingsInUI();

        // Then move focus away to another component so user has to manually select a keybind again (stopping any accidental changes)
        this.requestFocus();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        // Value in scalar spinner has changed, so update the value in the current settings
        currentSettings.screenScalar = (int) ((SpinnerModel) e.getSource()).getValue();
    }

    // Not interested in these listeners, but had to have them as they're in the interfaces we use

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}
}
