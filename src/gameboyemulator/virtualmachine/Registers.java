package gameboyemulator.virtualmachine;

public class Registers {
    // Some flags to determine which register(s) is being read
    public static final int A = 0;
    public static final int F = 1; // Flags register
    public static final int B = 2;
    public static final int C = 3;
    public static final int D = 4;
    public static final int E = 5;
    public static final int H = 6;
    public static final int L = 7;

    // 8 separate 8-bit registers
    public Registers() {
        eightBitRegisters = new int[8];

        eightBitRegisters[A] = 0x01;
        eightBitRegisters[F] = 0xB0;
        eightBitRegisters[B] = 0x00;
        eightBitRegisters[C] = 0x13;
        eightBitRegisters[D] = 0x00;
        eightBitRegisters[E] = 0xD8;
        eightBitRegisters[H] = 0x01;
        eightBitRegisters[L] = 0x4D;
    }

    // 8 bit registers
    int [] eightBitRegisters;

    // Interrupt Master Enable Flag
    boolean IME = true; // Enables/disables all interrupts

    // 16 bit registers
    int SP = 0xFFFE; // Stack Pointer - 0xFFFE on startup
    int PC = 0x0100; // Program Counter - start execution at address 0x0100

    // Methods for writing/reading the 8 8-bit registers
    int read(int register) {
        return eightBitRegisters[register] & 0xFF;
    }

    int readTwo(int register, int register2) {
        int regLeft = eightBitRegisters[register] & 0xFF;
        int regRight = eightBitRegisters[register2] & 0xFF;
        return (regRight | (regLeft << 8)) & 0xFFFF;
    }

    void write(int register, byte value) {
        eightBitRegisters[register] = value;
    }

    void write (int register, int value)     {
        write(register, (byte) (value & 0xFF));
    }

    void writeTwo(int register, int register2, int value) {
        write(register, value >> 8);
        write(register2, value);
    }

    void copyRegisterTo(int sourceRegister, int targetRegister) {
        eightBitRegisters[targetRegister] = eightBitRegisters[sourceRegister];
    }



    // Methods for math operations on double/combined registers

    void decrementDouble(int reg1, int reg2) {
        int combinedVal = readTwo(reg1, reg2);
        combinedVal--;
        writeTwo(reg1, reg2, combinedVal);
    }

    void incrementDouble(int reg1, int reg2) {
        int combinedVal = readTwo(reg1, reg2);
        combinedVal++;
        writeTwo(reg1, reg2, combinedVal);
    }





    // Methods for setting/resetting flags and reading their values easily

    // Flag register
    // Bit 7 = Z    Zero Flag
    // Bit 6 = N    Subtract Flag
    // Bit 5 = H    Half Carry Flag
    // Bit 4 = C    Carry Flag
    // Bits 3,2,1,0 unused

    void setFlagZero() {
        setFlagBit(7, 1);
    }

    void resetFlagZero() {
        setFlagBit(7, 0);
    }

    void setFlagSubtract() {
        setFlagBit(6, 1);
    }

    void resetFlagSubtract() {
        setFlagBit(6, 0);
    }

    void setFlagHalfCarry() {
        setFlagBit(5, 1);
    }

    void resetFlagHalfCarry() {
        setFlagBit(5, 0);
    }

    void setFlagCarry() {
        setFlagBit(4, 1);
    }

    void resetFlagCarry() {
        setFlagBit(4, 0);
    }

    void setAllFlags() {
        eightBitRegisters[Registers.F] = 0xF0;
    }

    void resetAllFlags() {
        eightBitRegisters[Registers.F] = 0x00;
    }

    void setFlagBit(int bit, int val) {
        setRegisterBit(Registers.F, bit, val);
    }

    void setRegisterBit(int register, int bit, int val) {
        // Note: the rightmost bit counts as bit 0, the leftmost bit in a byte counts as bit 7 (i.e. the bit is shifted 7 times over)
        // Val should be either 1 or 0, anything not 0 is treated as 1
        int mask = 0b1 << bit;

        int result = 0;
        if (val == 0)
            result =  eightBitRegisters[register] & ~mask;
        else
            result =  eightBitRegisters[register] | mask;

        eightBitRegisters[register] = 0xFF & result;
    }

    boolean flagZero() {
        int mask = 0b1 << 7;
        if ((eightBitRegisters[Registers.F] & mask) > 0)
            return true;
        else
            return false;
    }

    boolean flagSubtract() {
        int mask = 0b1 << 6;
        if ((eightBitRegisters[Registers.F] & mask) > 0)
            return true;
        else
            return false;
    }

    boolean flagHalfCarry() {
        int mask = 0b1 << 5;
        if ((eightBitRegisters[Registers.F] & mask) > 0)
            return true;
        else
            return false;
    }

    boolean flagCarry() {
        int mask = 0b1 << 4;
        if ((eightBitRegisters[Registers.F] & mask) > 0)
            return true;
        else
            return false;
    }
}