package gameboyemulator.virtualmachine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class DeviceInput implements KeyListener {

    // Flags to keep track of which boolean in the array corresponds to which Game Boy button
    private static final int KEY_DOWN_ARRAY_FLAG = 0;
    private static final int KEY_UP_ARRAY_FLAG = 1;
    private static final int KEY_LEFT_ARRAY_FLAG = 2;
    private static final int KEY_RIGHT_ARRAY_FLAG = 3;
    private static final int KEY_START_ARRAY_FLAG = 4;
    private static final int KEY_SELECT_ARRAY_FLAG = 5;
    private static final int KEY_B_ARRAY_FLAG = 6;
    private static final int KEY_A_ARRAY_FLAG = 7;

    // Flags for which real keybind is assigned to which Game Boy keypad button
    public static int KEYBIND_DOWN = KeyEvent.VK_DOWN;
    public static int KEYBIND_UP = KeyEvent.VK_UP;
    public static int KEYBIND_LEFT = KeyEvent.VK_LEFT;
    public static int KEYBIND_RIGHT = KeyEvent.VK_RIGHT;
    public static int KEYBIND_START = KeyEvent.VK_SPACE;
    public static int KEYBIND_SELECT = KeyEvent.VK_ENTER;
    public static int KEYBIND_B = KeyEvent.VK_B;
    public static int KEYBIND_A = KeyEvent.VK_A;

    // States of each button/key
    private boolean[] keyStatuses = new boolean[8];

    // JOYP is the register which contains information about which keys are pressed
    // Bits 6 and 7 aren't used, so just have them be set to 1 (as that's what the emulator BGB does)
    // Bits 4 and 5 are important here since they decide which buttons are currently represented in bites 0 through 3 when JOYP is read
    // Bits 0, 1, 2 and 3 represent which buttons on the Game Boy are currently pressed (bit = 0) or not pressed (bit = 1)
    private int JOYP = 0b11001111;

    private GameBoyCore gbCore;

    public DeviceInput(GameBoyCore gbInstance, int joypValue) {
        gbCore = gbInstance;
        JOYP = joypValue;
    }

    public boolean isKeyPressed(int keyVal) {
        return keyStatuses[keyVal];
    }

    public void writeToJOYP(int bits) {
        JOYP = bits | 0b11000000; // Bits 6 and 7 aren't used so have them always be 1
    }

    public int readFromJOYP() {
        int returnByte = JOYP & 0xF0;

        // Really a program can only get useful data from one flag OR the other being reset
        // But since it's possible to set both on a real Game Boy, allow it here for sake of realism

        // Check to see if bit 5 is reset which signifies the buttons start, select, A and B
        int selectKeysFlag = (JOYP >> 5) & 0b1;
        if (selectKeysFlag != 0b1) {
            if (GameBoyCore.VERBOSEDEBUGLOG)
                System.out.println("KEY SELECTION SELECT");

            returnByte = returnByte | buttonAndBitPosition(KEY_A_ARRAY_FLAG, 0);
            returnByte = returnByte | buttonAndBitPosition(KEY_B_ARRAY_FLAG, 1);
            returnByte = returnByte | buttonAndBitPosition(KEY_SELECT_ARRAY_FLAG, 2);
            returnByte = returnByte | buttonAndBitPosition(KEY_START_ARRAY_FLAG, 3);
        }

        // Check to see if bit 4 is reset which signifies the buttons up, down, left and right
        int directionKeysFlag = (JOYP >> 4) & 0b1;
        if (directionKeysFlag != 0b1) {
            if (GameBoyCore.VERBOSEDEBUGLOG)
                System.out.println("KEY SELECTION DIRECTIONS");

            returnByte = returnByte | buttonAndBitPosition(KEY_RIGHT_ARRAY_FLAG, 0);
            returnByte = returnByte | buttonAndBitPosition(KEY_LEFT_ARRAY_FLAG, 1);
            returnByte = returnByte | buttonAndBitPosition(KEY_UP_ARRAY_FLAG, 2);
            returnByte = returnByte | buttonAndBitPosition(KEY_DOWN_ARRAY_FLAG, 3);
        }

        return returnByte;
    }

    int buttonAndBitPosition(int buttonFlag, int bitPosition) {
        // If button is pressed, just return 0 (as flag for pressed button is 0)
        if (keyStatuses[buttonFlag])
            return 0;
        else
        // If button is not pressed, return 1 that's in the right position to be combined with other bits using logical OR
            return 0b1 << bitPosition;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Not needed
    }

    @Override
    public void keyPressed(KeyEvent e) {
        toggleKey(e, true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        toggleKey(e, false);
    }

    private void toggleKey(KeyEvent key, boolean val) {
        //System.out.println("Key " + key.getKeyChar() + "\t" + val);

        int keyCode = key.getKeyCode();

        // Work out if they key pressed was one we care about (i.e. one the keys bound to a Game Boy button)
        // Would be nicer to use a switch statement here, but since the KEYBIND values may change we can't
        int arrayFlag = -1;
        
        if (keyCode == KEYBIND_DOWN)
            arrayFlag = KEY_DOWN_ARRAY_FLAG;
        else if (keyCode == KEYBIND_UP)
            arrayFlag = KEY_UP_ARRAY_FLAG;
        else if (keyCode == KEYBIND_LEFT)
            arrayFlag = KEY_LEFT_ARRAY_FLAG;
        else if (keyCode == KEYBIND_RIGHT)
            arrayFlag = KEY_RIGHT_ARRAY_FLAG;
        else if (keyCode == KEYBIND_START)
            arrayFlag = KEY_START_ARRAY_FLAG;
        else if (keyCode == KEYBIND_SELECT)
            arrayFlag = KEY_SELECT_ARRAY_FLAG;
        else if (keyCode == KEYBIND_B)
            arrayFlag = KEY_B_ARRAY_FLAG;
        else if (keyCode == KEYBIND_A)
            arrayFlag = KEY_A_ARRAY_FLAG;
        
        if (arrayFlag == -1)
            // It was a key press we don't care about, so just return
            return;

        // If there's a change in the status of a button, then change it
        if (keyStatuses[arrayFlag] != val) {
            keyStatuses[arrayFlag] = val;
            
            // If we're going from high to low, fire a JOYP interrupt
            if (!val)
                gbCore.ram.requestInterrupt(Memory.InterruptJoypad);
        }

        // If the CPU is currently stopped then restart it
        if (gbCore.cpuAndLcdStopped)
            gbCore.cpuAndLcdStopped = false;
    }
}
