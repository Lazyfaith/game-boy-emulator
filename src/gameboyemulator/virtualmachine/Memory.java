package gameboyemulator.virtualmachine;

public class Memory {
    /*
        Memory Map (as defined in Game Boy CPU Manual)

        0xFFFF              Interrupt Enable Register
        0xFF80 - 0xFFFE     Internal RAM
        0xFF4C - 0xFF7F     Empty but unusable for I/O
        0xFF00 - 0xFF4B     I/O Ports
        0xFEA0 - 0xFEFF     Empty but unusable for I/O
        0xFE00 - 0xFE9F     Sprite Attrib Memory (OAM)
        0xE000 - 0xFDFF     Echo of 8kB Internal RAM (same as 0xC000 - 0xDE00, NB: since this area of ram is smaller than the 8kB internal ram, not the entire section is mirrored)
        0xC000 - 0xDFFF     8kB Internal RAM
        0xA000 - 0xBFFF     8kB switchable RAM bank
        0x8000 - 0x9FFF     Video RAM
        0x4000 - 0x7FFF     switchable ROM bank                 > - = 32kB cartridge (0x0000 - 0x7FFF)
        0x0000 - 0x3FFF     ROM bank #0                         > /



        Some important registers/values that are stored in actual memory:
        (ignoring those related to sound and serial data transfer as not implementing those in this emulator)

        Input
            0xFF00  P1/JOYP Joypad              Describes what buttons are currently being pressed, when changed can request a joypad interrupt

        Graphics
            0xFF40  LCDC                        Flags controlling LCD screen
            0xFF41  STAT    LCDC Status         Shows current status of the LCD controller?
            0xFF42  SCY     Scroll Y            Value to scroll screen Y position by
            0xFF43  SCX     Scroll X            Value to scroll screen X position by
            0xFF44  LY      LCDC Y-Coordinate   Indicates the line on the screen to which data is transferred to LCD driver
            0xFF45  LYC     LY Compare          LYC compares self with LY, if values are same then causes STAT to set coincident flag
            0xFF46  DMA                         DMA Transfer and Start Address (Page 55-56)
            0xFF47  BGP                         BG & Window Palette Data - selects shade of gray for background and window pixels
            0xFF48  OBP0                        Object Palette 0 data - selects colors for sprite 0
            0xFF49  OBP1                        Object Palette 1 data - selects colors for sprite 1
            0xFF4A  WY      Window Y Position   Must be 0 <= value <= 143 for window to be visible
            0xFF4B  WX      Window X Position   Must be 0 <= value <= 166 for window to be visible

        Timers
            0xFF04  DIV     Divider Register    Increment at rate of 16384Hz (256 slower than CPU tick speed), writing any value to this manually resets value to 0x00
            0xFF05  TIMA    Timer Counter       Increment at frequency specified by TAC. When this overflows (goes over 0xFF), reset to TMA value and request interrupt
            0xFF06  TMA     Timer Modula        When TIMA overflows, write this value to TIMA
            0xFF07  TAC     Timer Control       Bit 2 is Timer Stop (0=Stop 1=Start). Bits 1 and 0 are frequency to increment TIMA at (check manual for frequencies)

        Interrupts
            0xFF0F  IF      Interrupt Flag      Requests for interrupts to take place
            0xFFFF  IE      Interrupt Enable    What interrupts are currently enabled
     */

    // A few constants for addresses to important parts of memory
    public static final int AddressJOYP = 0xFF00;

    public static final int AddressLCDC = 0xFF40;
    public static final int AddressSTAT = 0xFF41;
    public static final int AddressSCY = 0xFF42;
    public static final int AddressSCX = 0xFF43;
    public static final int AddressLY = 0xFF44;
    public static final int AddressLYC = 0xFF45;
    public static final int AddressDMA = 0xFF46;
    public static final int AddressBGP = 0xFF47;
    public static final int AddressOBP0 = 0xFF48;
    public static final int AddressOBP1 = 0xFF49;
    public static final int AddressWY = 0xFF4A;
    public static final int AddressWX = 0xFF4B;

    public static final int AddressDIV = 0xFF04;
    public static final int AddressTIMA = 0xFF05;
    public static final int AddressTMA = 0xFF06;
    public static final int AddressTAC = 0xFF07;

    public static final int AddressIF = 0xFF0F;
    public static final int AddressIE = 0xFFFF;

    // Bitmasks signifying which bit is for which interrupt
    public static final int InterruptVBlank = 0b00000001;
    public static final int InterruptLCDSTAT = 0b00000010;
    public static final int InterruptTimer = 0b00000100;
    public static final int InterruptSerial = 0b00001000;
    public static final int InterruptJoypad = 0b00010000;

    int [] ram; // Using int here to make all the calculations/memory manipulation easier, though really we'll only use the last 8 bits of each
    private GameBoyCore gbCore;

    public Memory(GameBoyCore gbCoreInstance) {
        ram = new int[0xFFFF+1];// 0xFFFF is the last address possible

        // Reference to the GameBoyCore instance so we can access certain components
        gbCore = gbCoreInstance;

        // Assign the starting values to certain in memory registers
        writeByte(0xFF05, (byte) 0x00); // TIMA Timer Counter
        writeByte(0xFF06, (byte) 0x00); // TMA  Timer Modulo
        writeByte(0xFF07, (byte) 0xF8); // TAC  Timer Control
        writeByte(0xFF10, (byte) 0x80); // NR10
        writeByte(0xFF11, (byte) 0xBF); // NR11
        writeByte(0xFF12, (byte) 0xF3); // NR12
        writeByte(0xFF14, (byte) 0xBF); // NR14
        writeByte(0xFF16, (byte) 0x3F); // NR21
        writeByte(0xFF17, (byte) 0x00); // NR22
        writeByte(0xFF19, (byte) 0xBF); // NR24
        writeByte(0xFF1A, (byte) 0x7F); // NR30
        writeByte(0xFF1B, (byte) 0xFF); // NR31
        writeByte(0xFF1C, (byte) 0x9F); // NR32
        writeByte(0xFF1E, (byte) 0xBF); // NR33
        writeByte(0xFF20, (byte) 0xFF); // NR41
        writeByte(0xFF21, (byte) 0x00); // NR42
        writeByte(0xFF22, (byte) 0x00); // NR43
        writeByte(0xFF23, (byte) 0xBF); // NR30
        writeByte(0xFF24, (byte) 0x77); // NR50
        writeByte(0xFF25, (byte) 0xF3); // NR51
        writeByte(0xFF26, (byte) 0xF1); // NR52
        writeByte(0xFF40, (byte) 0x91); // LCDC LCD Controller
        writeByte(0xFF42, (byte) 0x00); // SCY  Scroll Y
        writeByte(0xFF43, (byte) 0x00); // SCX  Scroll x
        writeByte(0xFF45, (byte) 0x00); // LYC  LY Compare
        writeByte(0xFF47, (byte) 0xFC); // BGP  BG & Window Palette Data
        writeByte(0xFF48, (byte) 0xFF); // OBP0 Object Palette 0 data
        writeByte(0xFF49, (byte) 0xFF); // OBP1 Object Palette 1 data
        writeByte(0xFF4A, (byte) 0x00); // WY   Window Y Position
        writeByte(0xFF4B, (byte) 0x00); // WX   Window X Position
        writeByte(0xFFFF, (byte) 0x00); // IE   Interrupt Enable
     }

    public void writeByte(int address, byte value) {
        // Check if address being written to is special and must be implemented differently
        switch(address) {
            case AddressDIV:
                // Writing any value to this register will reset the value to 0x00
                if (GameBoyCore.VERBOSEDEBUGLOG)
                    System.out.println("SPECIAL WRITE to DIV (" + GameBoyCore.minLengthHexString(4, address) + ")\tValue\t" + GameBoyCore.minLengthHexString(2, 0));
                ram[address] = 0x00;
                return;
            case AddressDMA:
                // Instead of writing the val to memory, just immediately perform the DMA transfer
                // Slightly inaccurate as real GB takes 160 microseconds to do this,
                // shouldn't affect accuracy since how any game should wait that long or longer to do anything with this data regardless
                int sourceStart = (value << 8) & 0xFF00;
                for (int i=0x00; i <= 0x9F; i++) {
                    writeByte(0xFE00 + i, readByte(sourceStart + i));
                }
                return;
            case AddressSTAT:
                // Bits 0,1,2 are read only, so don't let these be overwritten
                if (GameBoyCore.VERBOSEDEBUGLOG)
                    System.out.println("SPECIAL WRITE to STAT(" + GameBoyCore.minLengthHexString(4, address) + ")\tValue\t" + GameBoyCore.minLengthHexString(2, combineWithCurrentVal(address, value, 0b11111000)));
                ram[address] = combineWithCurrentVal(address, value, 0b11111000);
                return;
            case AddressJOYP:
                // Special method for writing to JOYP Bits 0,1,2,3 are read only
                if (GameBoyCore.VERBOSEDEBUGLOG)
                    System.out.println("SPECIAL WRITE to JOYP (" + GameBoyCore.minLengthHexString(4, address) + ")\tValue\t" + GameBoyCore.minLengthHexString(2, value));
                gbCore.deviceInput.writeToJOYP(value);
                return;
            case AddressLY:
                // Read only, so just ignore this write instruction
                return;
        }

        // Write the given value to the address normally
        forceByte(address, value);
    }

    public int combineWithCurrentVal(int address, int newVal, int bitMask) {
        // Any bit that's set in the bitmask is writable, any bit that's not set is read only
        // Do not let the value of the read only bits be overwritten by the new data
        return ((newVal & bitMask) | (readByte(address) & ~bitMask)) & 0xFF;
    }

    public void writeByte(int address, int value) {
        writeByte(address, (byte) (value & 0xFF));
    }

    public void forceByte(int address, byte value) {
        // Writes bytes to RAM without the consideration for treating certain bytes differently (save for mirroring certain part of memory )

        if (GameBoyCore.VERBOSEDEBUGLOG)
            System.out.println("Write to " + GameBoyCore.minLengthHexString(4, address) + "\tValue\t" + GameBoyCore.minLengthHexString(2, value));
        ram[address] = value;

        // Mirrored RAM writing (anything written in 0xC000 - 0xDDFF is written in 0xE000 - 0xFDFF too, and vice versa)
        if (address >= 0xC000 && address <= 0xDDFF) {
            ram[address + 8192] = value;
        }
        else if (address >= 0xE000 && address <= 0xFDFF) {
            ram[address - 8192] = value;
        }

        if (address >= 0x8000 && address <= 0x97FF) {
            // This is tile data being written, so also update the tiles cache in the GPU
            gbCore.screen.updateTile(address);
        } else if (address >= 0xFE00 && address <= 0xFE9F) {
            // This is sprite data being written, so also update the spriteCache in the GPU
            gbCore.screen.updateSprite(address, value & 0xFF);
        }
    }

    public void forceByte(int address, int value) {
        forceByte(address, (byte) (value & 0xFF));
    }

    public int readByte(int address) {
        // JOYP register has special read method
        if (address == AddressJOYP)
            return gbCore.deviceInput.readFromJOYP() & 0xFF;

        return ram[address] & 0xFF;
    }

    public int readTwoBytes(int address) {
        return (readByte(address+1) | readByte(address) << 8) & 0xFFFF;
    }

    public boolean anyInterruptsRequestedAndEnabled() {
        // Note: This only checks to see if any specific interrupts have been REQUESTED AND ENABLED
        // Interrupts should only be acted on if the IME flag is also set (held in the Registers class)
        int temp = (readByte(Memory.AddressIF) & readByte(Memory.AddressIE)) & 0x1F;
        if (temp > 0)
            return true;
        else
            return false;
    }

    public void requestInterrupt(int bitMask) {
        forceByte(AddressIF, readByte(AddressIF) | bitMask);
    }

    public void unrequestInterrupt(int bitMask) {
        forceByte(AddressIF, readByte(AddressIF) & ~bitMask);
    }

    public void enableInterrupt(int bitMask) {
        forceByte(AddressIE, readByte(AddressIE) | bitMask);
    }

    public void disableInterrupt(int bitMask) {
        forceByte(AddressIE, readByte(AddressIE) & ~bitMask);
    }

    public boolean interruptEnabledAndRequested(int bitMask) {
        int temp = (readByte(Memory.AddressIE) & readByte(Memory.AddressIF)) & bitMask;
        if (temp > 0)
            return true;
        else
            return false;
    }
}
