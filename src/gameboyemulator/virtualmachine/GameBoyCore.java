package gameboyemulator.virtualmachine;

import gameboyemulator.ImportantStrings;
import gameboyemulator.SaveState;

import javax.swing.*;
import java.io.*;

/*
    NOTA BENE: This class and others like it (i.e. Memory, Registers, Screen) are commented in a way that is relevant.
                One should have at least a rudimentary knowledge of emulation, binary manipulation, CPUs and to a lesser extent Game Boy emulation specifically.
                I will not waste time/space here by explaining simple things such as how an emulator works, what bit shifting is, etc.
                Game Boy specific information can be found from various online resources, most notably the Game Boy CPU Manual.
 */

public class GameBoyCore {

    // RAM
    Memory ram;

    // Registers
    Registers registers = new Registers();

    // VM components
    public Screen screen = null;
    public DeviceInput deviceInput = null;

    // Emulation related vars
    public static final boolean VERBOSEDEBUGLOG = false;
    boolean romLoaded = false;
    boolean running = false;
    long nanoTimeLastCpuTick = 0;
    int cyclesToDelay = 0;
    long cycleCountLastTimersUpdate = 0;
    long divCyclesSinceLastInc = 0;
    long timaCyclesSinceLastInc = 0;
    long stepCount = 0;
    long cpuCyclesPassed = 0;
    boolean imeChangeDelayed = false;
    boolean activeIME = true; // This is what should generally be referenced by interrupts instead of the actual IME register as this has the proper delay on it's changes
    boolean cpuHalted = false;
    boolean cpuAndLcdStopped = false;
    boolean failedHalt = false;

    /*
        Timings in Gameboy CPU Manual and in this program are handled in terms of clock cycles
        CPU goes at 4.194304MHz in terms of clock cycles

        ( Millisecond = 1,000th of a second )
        ( Nanosecond = 1,000,000th of a millisecond )

        1 second =  4,191,304 clock cycles
        1 millisecond = 4,191.304 clock cycles

        1 clock cycle = 238.4185791015625 nanoseconds
                      = 0.0002384185791015625 milliseconds

        All timings in this program should be based off the variables cyclesToDelay and cpuCyclesPassed, NOT stepCount.
     */

    public GameBoyCore() {
        // Prepare screen
        screen = new Screen(this);

        // Set emulator to clean start
        hardReset();

        System.out.println("Core initialised");
    }

    public void hardReset() {
        // Reset everything in the emulator that affects how it runs to their default values

        ram = new Memory(this);
        screen.resetGpu();
        registers = new Registers();
        deviceInput = new DeviceInput(this, 0b11001111);

        romLoaded = false;
        running = false;
        nanoTimeLastCpuTick = 0;
        cyclesToDelay = 0;
        cycleCountLastTimersUpdate = 0;
        divCyclesSinceLastInc = 0;
        timaCyclesSinceLastInc = 0;
        stepCount = 0;
        cpuCyclesPassed = 0;
        imeChangeDelayed = false;
        activeIME = true;
        cpuHalted = false;
        cpuAndLcdStopped = false;
        failedHalt = false;
    }

    public void loadRom(File rom) throws IOException {
        System.out.println("Beginning to read ROM...");
        FileInputStream reader = new FileInputStream(rom);
        int byteRead = 0;
        int counter = 0;
        while ((byteRead = reader.read()) != -1) {
            ram.writeByte(counter++, (byte) byteRead);
        }
        romLoaded = true;
        System.out.println("ROM read into memory - " + counter + " bytes read");
        System.out.println("Game Title: " + extractGameTitle());
    }

    public void loadState(SaveState state) {
        System.out.println("Loading save state data into current VM...");

        // Registers
        registers.write(Registers.A, state.registerA);
        registers.write(Registers.F, state.registerF);
        registers.write(Registers.B, state.registerB);
        registers.write(Registers.C, state.registerC);
        registers.write(Registers.D, state.registerD);
        registers.write(Registers.E, state.registerE);
        registers.write(Registers.H, state.registerH);
        registers.write(Registers.L, state.registerL);

        registers.IME = state.IME;
        registers.SP = state.SP;
        registers.PC = state.PC;

        // Emulation related variables
        romLoaded = state.romLoaded;
        cycleCountLastTimersUpdate = state.cycleCountLastTimersUpdate;
        divCyclesSinceLastInc = state.divCyclesSinceLastInc;
        timaCyclesSinceLastInc = state.timaCyclesSinceLastInc;
        stepCount = state.stepCount;
        cpuCyclesPassed = state.cpuCyclesPassed;
        imeChangeDelayed = state.imeChangeDelayed;
        activeIME = state.activeIME;
        cpuHalted = state.cpuHalted;
        cpuAndLcdStopped = state.cpuAndLcdStopped;
        failedHalt = state.failedHalt;

        // RAM
        for (int i=0; i<state.ram.length; i++)
            ram.forceByte(i, state.ram[i]);
        // JOYP value gets processed by DevinceInput via normal write method
        ram.writeByte(Memory.AddressJOYP, state.ram[Memory.AddressJOYP]);

        romLoaded = true;
    }

    public SaveState generateSaveState() {
        System.out.println("Generating saved state based on current VM...");
        SaveState currentState = new SaveState();

        // Registers
        currentState.registerA = registers.read(Registers.A);
        currentState.registerF = registers.read(Registers.F);
        currentState.registerB = registers.read(Registers.B);
        currentState.registerC = registers.read(Registers.C);
        currentState.registerD = registers.read(Registers.D);
        currentState.registerE = registers.read(Registers.E);
        currentState.registerH = registers.read(Registers.H);
        currentState.registerL = registers.read(Registers.L);

        currentState.IME = registers.IME;
        currentState.SP = registers.SP;
        currentState.PC = registers.PC;

        // Emulation related variables
        currentState.romLoaded = romLoaded;
        currentState.cycleCountLastTimersUpdate = cycleCountLastTimersUpdate;
        currentState.divCyclesSinceLastInc = divCyclesSinceLastInc;
        currentState.timaCyclesSinceLastInc = timaCyclesSinceLastInc;
        currentState.stepCount = stepCount;
        currentState.cpuCyclesPassed = cpuCyclesPassed;
        currentState.imeChangeDelayed = imeChangeDelayed;
        currentState.activeIME = activeIME;
        currentState.cpuHalted = cpuHalted;
        currentState.cpuAndLcdStopped = cpuAndLcdStopped;
        currentState.failedHalt = failedHalt;

        // RAM
        currentState.ram = ram.ram.clone();

        return currentState;
    }

    private String extractGameTitle() {
        // The title of the game is held in memory locations 0x0134 - 0x0142 in ASCII format
        String gameTitle = "";
        for(int i=0; i<16; i++) {
            gameTitle += (char) ram.readByte(0x0134 + i);
        }
        return gameTitle;
    }

    public boolean isRomLoaded() {
        return romLoaded;
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        System.out.println("VM requested to stop..");
        running = false;
    }

    public void start() {
        if (!romLoaded) {
            JOptionPane.showMessageDialog(null, "You must load a ROM before starting the emulator.", ImportantStrings.MAINWINDOWTITLE, JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        System.out.println("Starting VM...");
        running = true;
        startContinuousTickCycle();
    }

    void startContinuousTickCycle() {
        int targetClockCyclesPerSecond = 4191304;
        long lastTimeLog = System.currentTimeMillis();
        long cyclesLastTimeLog = 0;

        while(running) {
            stepCount++;
            singleTick();

            long currentTime = System.currentTimeMillis();
            if (currentTime >= (lastTimeLog + 1000)) {
                long cyclesInLastSecond = (cpuCyclesPassed - cyclesLastTimeLog);
                System.out.println(cyclesInLastSecond + " cycles in last second, " + ((float) cyclesInLastSecond / (float) targetClockCyclesPerSecond) + " target speed.");
                lastTimeLog = System.currentTimeMillis();
                cyclesLastTimeLog = cpuCyclesPassed;
            }
        }
        System.out.println("VM stopped");
    }

    public void singleTick() {
        // Tick at 4.194304 MHz (i.e. 4194304Hz, 4194304 ticks per second, 238.4185791015625 nanoseconds per tick/cycle)
        if (System.nanoTime() >= nanoTimeLastCpuTick + (cyclesToDelay * 238.4185791015625)) {
            // If CPU/LCD have been stopped by STOP command then don't do anything until a button press starts it again
            if (cpuAndLcdStopped)
                return;

            nanoTimeLastCpuTick = System.nanoTime();
            cyclesToDelay = 0;

            // Handle the next opcode (as long as the CPU is not halted)
            if (!cpuHalted)
                opcode();
            else
                // CPU is halted, so tick along the GPU by 4 cycles each time
                cyclesToDelay += 4;

            cpuCyclesPassed += cyclesToDelay;

            // If the IME has just been changed by an opcode, then that change shouldn't be moved to the activeIME until after next opcode processed
            // If the IME hasn't just been changed, the activeIME variable should match the IME register
            if (imeChangeDelayed)
                imeChangeDelayed = false;
            else
                activeIME = registers.IME;

            // Progress GPU in time with number of CPU cycles passed
            screen.gpuStep(cpuCyclesPassed);

            // Update timers
            updateTimers();

            // Interrupts
            // Check if IME is on (Interrupt Master Enable)
            // and check if any specific interrupt has been enabled (via IE register) and has been requested (via IF register)
            if (activeIME && ram.anyInterruptsRequestedAndEnabled()) {
                /*
                The interrupt priority is the same as they're handled below i.e. VBlank is first and has highest priority

                Interrupts implemented:
                    Timer
                    V-Blank
                    Joypad

                Still need:
                    LCD STAT

                Serial interrupt not needed (as that's for serial data transfer over link cable, not implemented in this emulator)
                 */
                // Handling an interrupt here so make sure to unhalt the CPU if needed
                cpuHalted = false;

                if (VERBOSEDEBUGLOG) {
                    System.out.println("INTERRUPT HAS OCCURRED");
                    System.out.println("IME\tTrue\tIE\t" + minLengthHexString(2, ram.readByte(Memory.AddressIE)) + "\tIF\t" + minLengthHexString(2, ram.readByte(Memory.AddressIF)));
                    //debugWarningPopup("Interrupt has occurred");
                }

                boolean interruptOccurs = false;

                // VBlank
                if (ram.interruptEnabledAndRequested(Memory.InterruptVBlank)) {
                    // Set/reset relative flags
                    setBothImes(false);
                    ram.unrequestInterrupt(Memory.InterruptVBlank);
                    interruptOccurs = true;

                    // Push current PC to stack and then change PC to interrupts jump address
                    pushTwoBytesToStack(registers.PC);
                    registers.PC = 0x40;
                }

                // LCD STAT
                if (activeIME && ram.interruptEnabledAndRequested(Memory.InterruptLCDSTAT)) {
                    setBothImes(false);
                    ram.unrequestInterrupt(Memory.InterruptLCDSTAT);
                    interruptOccurs = true;

                    pushTwoBytesToStack(registers.PC);
                    registers.PC = 0x48;
                }

                // Timer
                if (activeIME && ram.interruptEnabledAndRequested(Memory.InterruptTimer)) {
                    setBothImes(false);
                    ram.unrequestInterrupt(Memory.InterruptTimer);
                    interruptOccurs = true;

                    pushTwoBytesToStack(registers.PC);
                    registers.PC = 0x50;
                }

                // Serial
                if (activeIME && ram.interruptEnabledAndRequested(Memory.InterruptSerial)) {
                    /* This emulator isn't emulating data transfers over link cable, so this interrupt will only occur if the
                     * ROM code manually calls it */
                    setBothImes(false);
                    ram.unrequestInterrupt(Memory.InterruptSerial);
                    interruptOccurs = true;

                    pushTwoBytesToStack(registers.PC);
                    registers.PC = 0x58;
                }

                // Joypad
                if (activeIME && ram.interruptEnabledAndRequested(Memory.InterruptJoypad)) {
                    setBothImes(false);
                    ram.unrequestInterrupt(Memory.InterruptJoypad);
                    interruptOccurs = true;

                    pushTwoBytesToStack(registers.PC);
                    registers.PC = 0x60;
                }

                if (interruptOccurs) {
                    // Takes 12 clock cycles to handle the interrupt and jump to it's memory location, so timers need updating
                    cpuCyclesPassed += 12;
                    screen.gpuStep(cpuCyclesPassed);
                    updateTimers();
                }
            }
        }
    }

    void setBothImes(boolean val) {
        activeIME = val;
        registers.IME = val;
    }


    void updateTimers() {
        // Timers should all be updated based on the CPU number of cycles passed, NOT by real time passed
        // This is so timers are still accurate when pausing emulator or running it 1 CPU step at a time
        long cyclesPassed = cpuCyclesPassed - cycleCountLastTimersUpdate;
        cycleCountLastTimersUpdate = cpuCyclesPassed;

        // Increment DIV 16384 times a second (once every 256 cycles)
        divCyclesSinceLastInc += cyclesPassed;
        if (divCyclesSinceLastInc >= 256) {
            ram.forceByte(Memory.AddressDIV, ram.readByte(Memory.AddressDIV)+1);
            divCyclesSinceLastInc -= 256;
        }

        // Check if TIMA is running (by looking at TAC)
        if (((ram.readByte(Memory.AddressTAC) >> 2) & 0b1) == 0b1) {
            // Timer is running, get number of cycles between each increment
            int freq = ram.readByte(Memory.AddressTAC) & 0b11;
            int cycleGap = 0;
            switch(freq) {
                case 0b00: cycleGap = 1024; break;
                case 0b01: cycleGap = 16; break;
                case 0b10: cycleGap = 64; break;
                case 0b11: cycleGap = 256; break;
            }

            // Check if time to increment TIMA
            timaCyclesSinceLastInc += cyclesPassed;
            if (timaCyclesSinceLastInc >= cycleGap) {
                int timaVal = ram.readByte(Memory.AddressTIMA) + 1;
                ram.forceByte(Memory.AddressTIMA, timaVal);
                timaCyclesSinceLastInc = 0;

                // Check if TIMA has now overflowed
                if (timaVal > 0xFF) {
                    // Reset value to value of TMA
                    ram.forceByte(Memory.AddressTIMA, ram.readByte(Memory.AddressTMA));
                    // Request a timer interrupt
                    ram.requestInterrupt(Memory.InterruptTimer);
                }
            }
        }
    }

    void opcode() {
        // Read the next opcode pointed to by the PC
        int opcode = ram.readByte(registers.PC);
        if (failedHalt) {
            // Last opcode to run was a HALT instruction that didn't take affect due to interrupts being disabled
            // This causes the following byte to be read twice, so decrement PC by 1 here to do that
            failedHalt = false;
            registers.PC -= 1;
        }
        // And read the bytes after the opcode (as these are commonly used as parameters for the opcode)
        int immediateByte = ram.readByte(registers.PC + 1);
        int immediateTwoBytes = 0;
        if (registers.PC < 0xFFFF)
            // Last memory address is 0xFFFF, so if PC is 0xFFFF then there isn't a second byte after it to read
            immediateTwoBytes =ram.readTwoBytes(registers.PC + 1);
        int immediateTwoBytesSwapped = swapTwoBytes(immediateTwoBytes);

        if (VERBOSEDEBUGLOG)
            System.out.println(minLengthHexString(4, registers.PC) + "\t" + minLengthHexString(2, opcode) +"\t(" + stepCount + " ticks, " + cpuCyclesPassed + " real cycles)");

//        String debugString = "PC      " + minLengthHexString(4, registers.PC)
//                + "\nOpcode      " + minLengthHexString(2, opcode)
//                + "\nNext 2 bytes      " + minLengthHexString(4, immediateTwoBytes)
//                + "\n\nAF      " + minLengthHexString(4, registers.readTwo(Registers.A, Registers.F))
//                + "\nBC      " + minLengthHexString(4, registers.readTwo(Registers.B, Registers.C))
//                + "\nDE      " + minLengthHexString(4, registers.readTwo(Registers.D, Registers.E))
//                + "\nHL      " + minLengthHexString(4, registers.readTwo(Registers.H, Registers.L))
//                + "\n\nSP      " + minLengthHexString(4, registers.SP)
//                + "\n\nDIV   " + minLengthHexString(2, ram.readByte(Memory.AddressDIV))
//                + "\nTIMA  " + minLengthHexString(2, ram.readByte(Memory.AddressTIMA))
//                + "\n\nIME     " + Boolean.toString(registers.IME) + " aIME " + Boolean.toString(activeIME)
//                + "\nIE        " + minLengthHexString(2, ram.readByte(Memory.AddressIE))
//                + "\nIF        " + minLengthHexString(2, ram.readByte(Memory.AddressIF))
//                + "\nOverlap?  " + Boolean.toString(ram.anyInterruptsRequestedAndEnabled())
//                + "\n\nFlags"
//                + "\nZ " + registers.flagZero()
//                + "\nN " + registers.flagSubtract()
//                + "\nH " + registers.flagHalfCarry()
//                + "\nC " + registers.flagCarry()
//                + "\n\n(HL) " + minLengthHexString(2, readRamAtRegs(Registers.H, Registers.L))
//                + "\n\nLCDC " + minLengthHexString(2, ram.readByte(Memory.AddressLCDC))
//                + "\nSTAT " + minLengthHexString(2, ram.readByte(Memory.AddressSTAT))
//                + "\nLY " + minLengthHexString(2, ram.readByte(Memory.AddressLY));


        // It seems like the PC is incremented by 1 after the opcode is read and before the opcode is executed
        // Any opcodes with operands following the opcode will have to increment the PC themselves for those operands
        // If an opcodes action is based off the PC, it should increment the PC equal to the operands before acting
        registers.PC++;

        // The comments about the opcodes below separate them into categories with pages for each category
        // These page numbers are the pages in the Game Boy CPU Manual that these opcodes are described on
        // The comments here fore the different opcodes are extremely similar to the text in
        //  the Game Boy CPU Manual that describes what each opcode/command does.

// @formatter:off
        switch (opcode) {
        // 8-Bit Loads          Pgs 65-75   20 Commands     ////////////////////////////////////////////////////////////
            // 1. LD nn,n
            case 0x06: registers.write(Registers.B, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            case 0x0E: registers.write(Registers.C, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            case 0x16: registers.write(Registers.D, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            case 0x1E: registers.write(Registers.E, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            case 0x26: registers.write(Registers.H, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            case 0x2E: registers.write(Registers.L, immediateByte); cyclesToDelay += 8; registers.PC += 1; break;
            // 2. LD r1, r2 - Put value r2 into r1
            // Note: Omitted first 8 opcodes that have A as r1 as they are identical to LD A,n opcodes
            case 0x40: registers.copyRegisterTo(Registers.B, Registers.B); cyclesToDelay += 4; break;
            case 0x41: registers.copyRegisterTo(Registers.C, Registers.B); cyclesToDelay += 4; break;
            case 0x42: registers.copyRegisterTo(Registers.D, Registers.B); cyclesToDelay += 4; break;
            case 0x43: registers.copyRegisterTo(Registers.E, Registers.B); cyclesToDelay += 4; break;
            case 0x44: registers.copyRegisterTo(Registers.H, Registers.B); cyclesToDelay += 4; break;
            case 0x45: registers.copyRegisterTo(Registers.L, Registers.B); cyclesToDelay += 4; break;
            case 0x46: registers.write(Registers.B, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x48: registers.copyRegisterTo(Registers.B, Registers.C); cyclesToDelay += 4; break;
            case 0x49: registers.copyRegisterTo(Registers.C, Registers.C); cyclesToDelay += 4; break;
            case 0x4A: registers.copyRegisterTo(Registers.D, Registers.C); cyclesToDelay += 4; break;
            case 0x4B: registers.copyRegisterTo(Registers.E, Registers.C); cyclesToDelay += 4; break;
            case 0x4C: registers.copyRegisterTo(Registers.H, Registers.C); cyclesToDelay += 4; break;
            case 0x4D: registers.copyRegisterTo(Registers.L, Registers.C); cyclesToDelay += 4; break;
            case 0x4E: registers.write(Registers.C, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x50: registers.copyRegisterTo(Registers.B, Registers.D); cyclesToDelay += 4; break;
            case 0x51: registers.copyRegisterTo(Registers.C, Registers.D); cyclesToDelay += 4; break;
            case 0x52: registers.copyRegisterTo(Registers.D, Registers.D); cyclesToDelay += 4; break;
            case 0x53: registers.copyRegisterTo(Registers.E, Registers.D); cyclesToDelay += 4; break;
            case 0x54: registers.copyRegisterTo(Registers.H, Registers.D); cyclesToDelay += 4; break;
            case 0x55: registers.copyRegisterTo(Registers.L, Registers.D); cyclesToDelay += 4; break;
            case 0x56: registers.write(Registers.D, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x58: registers.copyRegisterTo(Registers.B, Registers.E); cyclesToDelay += 4; break;
            case 0x59: registers.copyRegisterTo(Registers.C, Registers.E); cyclesToDelay += 4; break;
            case 0x5A: registers.copyRegisterTo(Registers.D, Registers.E); cyclesToDelay += 4; break;
            case 0x5B: registers.copyRegisterTo(Registers.E, Registers.E); cyclesToDelay += 4; break;
            case 0x5C: registers.copyRegisterTo(Registers.H, Registers.E); cyclesToDelay += 4; break;
            case 0x5D: registers.copyRegisterTo(Registers.L, Registers.E); cyclesToDelay += 4; break;
            case 0x5E: registers.write(Registers.E, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x60: registers.copyRegisterTo(Registers.B, Registers.H); cyclesToDelay += 4; break;
            case 0x61: registers.copyRegisterTo(Registers.C, Registers.H); cyclesToDelay += 4; break;
            case 0x62: registers.copyRegisterTo(Registers.D, Registers.H); cyclesToDelay += 4; break;
            case 0x63: registers.copyRegisterTo(Registers.E, Registers.H); cyclesToDelay += 4; break;
            case 0x64: registers.copyRegisterTo(Registers.H, Registers.H); cyclesToDelay += 4; break;
            case 0x65: registers.copyRegisterTo(Registers.L, Registers.H); cyclesToDelay += 4; break;
            case 0x66: registers.write(Registers.H, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x68: registers.copyRegisterTo(Registers.B, Registers.L); cyclesToDelay += 4; break;
            case 0x69: registers.copyRegisterTo(Registers.C, Registers.L); cyclesToDelay += 4; break;
            case 0x6A: registers.copyRegisterTo(Registers.D, Registers.L); cyclesToDelay += 4; break;
            case 0x6B: registers.copyRegisterTo(Registers.E, Registers.L); cyclesToDelay += 4; break;
            case 0x6C: registers.copyRegisterTo(Registers.H, Registers.L); cyclesToDelay += 4; break;
            case 0x6D: registers.copyRegisterTo(Registers.L, Registers.L); cyclesToDelay += 4; break;
            case 0x6E: registers.write(Registers.L, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0x70: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.B)); cyclesToDelay += 8; break;
            case 0x71: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.C)); cyclesToDelay += 8; break;
            case 0x72: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.D)); cyclesToDelay += 8; break;
            case 0x73: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.E)); cyclesToDelay += 8; break;
            case 0x74: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.H)); cyclesToDelay += 8; break;
            case 0x75: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.L)); cyclesToDelay += 8; break;
            case 0x36: writeRamAtRegs(Registers.H, Registers.L, immediateByte); registers.PC++; cyclesToDelay += 12; break;
            // 3. LD A,n - Put value n into A
            case 0x7F: /* NOP - writing register A into register A does nothing*/ cyclesToDelay += 4; break;
            case 0x78: registers.copyRegisterTo(Registers.B, Registers.A); cyclesToDelay += 4; break;
            case 0x79: registers.copyRegisterTo(Registers.C, Registers.A); cyclesToDelay += 4; break;
            case 0x7A: registers.copyRegisterTo(Registers.D, Registers.A); cyclesToDelay += 4; break;
            case 0x7B: registers.copyRegisterTo(Registers.E, Registers.A); cyclesToDelay += 4; break;
            case 0x7C: registers.copyRegisterTo(Registers.H, Registers.A); cyclesToDelay += 4; break;
            case 0x7D: registers.copyRegisterTo(Registers.L, Registers.A); cyclesToDelay += 4; break;
            case 0x0A: registers.write(Registers.A, readRamAtRegs(Registers.B, Registers.C)); cyclesToDelay += 8; break;
            case 0x1A: registers.write(Registers.A, readRamAtRegs(Registers.D, Registers.E)); cyclesToDelay += 8; break;
            case 0x7E: registers.write(Registers.A, readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0xFA: registers.write(Registers.A, ram.readByte(immediateTwoBytesSwapped)); registers.PC+=2; cyclesToDelay += 16; break;
            case 0x3E: registers.write(Registers.A, immediateByte); registers.PC+=1; cyclesToDelay += 8; break;
            // 4. LD n,A - Put value A into n
            // case 0x7F: NOP and also already handled elsewhere.
            case 0x47: registers.copyRegisterTo(Registers.A, Registers.B); cyclesToDelay += 4; break;
            case 0x4F: registers.copyRegisterTo(Registers.A, Registers.C); cyclesToDelay += 4; break;
            case 0x57: registers.copyRegisterTo(Registers.A, Registers.D); cyclesToDelay += 4; break;
            case 0x5F: registers.copyRegisterTo(Registers.A, Registers.E); cyclesToDelay += 4; break;
            case 0x67: registers.copyRegisterTo(Registers.A, Registers.H); cyclesToDelay += 4; break;
            case 0x6F: registers.copyRegisterTo(Registers.A, Registers.L); cyclesToDelay += 4; break;
            case 0x02: writeRamAtRegs(Registers.B, Registers.C, registers.read(Registers.A)); cyclesToDelay += 8; break;
            case 0x12: writeRamAtRegs(Registers.D, Registers.E, registers.read(Registers.A)); cyclesToDelay += 8; break;
            case 0x77: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.A)); cyclesToDelay += 8; break;
            case 0xEA: ram.writeByte(immediateTwoBytesSwapped, registers.read(Registers.A)); registers.PC += 2; cyclesToDelay += 16; break;
            // 5. LD A,(C) - Put value at address 0xFF00+C into A
            case 0xF2: registers.write(Registers.A, ram.readByte(0xFF00 + registers.read(Registers.C))); cyclesToDelay += 8; break;
            // 6. LD (C),A - Put value of A into address 0xFF00 + register C
            case 0xE2: ram.writeByte(0xFF00 + registers.read(Registers.C), registers.read(Registers.A)); cyclesToDelay += 8; break;
            // 7&8&9. LDD A,(HL) - Put value at address HL into A then decrement HL
            case 0x3A: registers.write(Registers.A, readRamAtRegs(Registers.H, Registers.L)); registers.decrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            // 10&11&12. LDD (HL), A - Put A into memory address HL and then decrement HL
            case 0x32: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.A)); registers.decrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            // 13&14&15. LDI A,(HL) - Put (HL) into A then increment HL
            case 0x2A: registers.write(Registers.A, readRamAtRegs(Registers.H, Registers.L)); registers.incrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            // 16&17&18. LDI (HL), A - Put A into (HL) then increment HL
            case 0x22: writeRamAtRegs(Registers.H, Registers.L, registers.read(Registers.A)); registers.incrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            // 19. LDH (n), A - Put A into memory address 0xFF00 + n
            case 0xE0: ram.writeByte(0xFF00 + immediateByte, registers.read(Registers.A)); registers.PC += 1; cyclesToDelay += 12; break;
            // 20. LDH A,(n) - Put memory address 0xFF00 + n into register A
            case 0xF0: registers.write(Registers.A, ram.readByte(0xFF00 + immediateByte)); registers.PC += 1; cyclesToDelay += 12; break;

        // 16-Bit Loads         Pgs 76-79   7  Commands     ////////////////////////////////////////////////////////////
            // 1. LD n,nn - Put value nn into register n
            case 0x01: registers.writeTwo(Registers.B, Registers.C, immediateTwoBytesSwapped); cyclesToDelay += 12; registers.PC+=2; break;
            case 0x11: registers.writeTwo(Registers.D, Registers.E, immediateTwoBytesSwapped); cyclesToDelay += 12; registers.PC+=2; break;
            case 0x21: registers.writeTwo(Registers.H, Registers.L, immediateTwoBytesSwapped); cyclesToDelay += 12; registers.PC+=2; break;
            case 0x31: registers.SP = immediateTwoBytesSwapped; cyclesToDelay+= 12; registers.PC+=2; break;
            // 2. LD SP,HL - Pur HL into SP
            case 0xF9: registers.SP = registers.readTwo(Registers.H, Registers.L); cyclesToDelay += 8; break;
            // 3&4. LDHL SP,n - Put SP + n into HL
            case 0xF8: addSignedByteToSPWithFlagChanges(immediateByte); registers.PC += 1; cyclesToDelay += 12; break;
            // 5. LD (nn),SP - Put SP at address n
            case 0x08: ram.writeByte(immediateTwoBytesSwapped, registers.SP); ram.writeByte(immediateTwoBytesSwapped+1, registers.SP >> 8); registers.PC += 2; cyclesToDelay += 20; break;
            // 6. PUSH nn - Push register pair nn onto stack, decrement SP twice
            case 0xF5: pushTwoBytesToStack(registers.readTwo(Registers.A, Registers.F)); cyclesToDelay += 16; break;
            case 0xC5: pushTwoBytesToStack(registers.readTwo(Registers.B, Registers.C)); cyclesToDelay += 16; break;
            case 0xD5: pushTwoBytesToStack(registers.readTwo(Registers.D, Registers.E)); cyclesToDelay += 16; break;
            case 0xE5: pushTwoBytesToStack(registers.readTwo(Registers.H, Registers.L)); cyclesToDelay += 16; break;
            // 7. POP nn - Pop 2 bytes from stack into register pair nn, increment SP twice
            case 0xF1: registers.writeTwo(Registers.A, Registers.F, popTwoBytesFromStack()); cyclesToDelay += 12; break;
            case 0xC1: registers.writeTwo(Registers.B, Registers.C, popTwoBytesFromStack()); cyclesToDelay += 12; break;
            case 0xD1: registers.writeTwo(Registers.D, Registers.E, popTwoBytesFromStack()); cyclesToDelay += 12; break;
            case 0xE1: registers.writeTwo(Registers.H, Registers.L, popTwoBytesFromStack()); cyclesToDelay += 12; break;

        // 8-Bit ALU            Pgs 80-89   10 Commands     ////////////////////////////////////////////////////////////
            // 1. ADD A,n - Add n to A
            case 0x87: addRegister(Registers.A, 4); break;
            case 0x80: addRegister(Registers.B, 4); break;
            case 0x81: addRegister(Registers.C, 4); break;
            case 0x82: addRegister(Registers.D, 4); break;
            case 0x83: addRegister(Registers.E, 4); break;
            case 0x84: addRegister(Registers.H, 4); break;
            case 0x85: addRegister(Registers.L, 4); break;
            case 0x86: addValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xC6: addValue(immediateByte, 8); registers.PC++; break;
            // 2. ADC A,n - Add n + C flag to A
            case 0x8F: adcRegister(Registers.A); cyclesToDelay += 4; break;
            case 0x88: adcRegister(Registers.B); cyclesToDelay += 4; break;
            case 0x89: adcRegister(Registers.C); cyclesToDelay += 4; break;
            case 0x8A: adcRegister(Registers.D); cyclesToDelay += 4; break;
            case 0x8B: adcRegister(Registers.E); cyclesToDelay += 4; break;
            case 0x8C: adcRegister(Registers.H); cyclesToDelay += 4; break;
            case 0x8D: adcRegister(Registers.L); cyclesToDelay += 4; break;
            case 0x8E: adcValue(readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0xCE: adcValue(immediateByte); registers.PC += 1; cyclesToDelay += 8; break;
            // 3. SUB n - Subtract n from A
            case 0x97: subtractRegister(Registers.A, 4); break;
            case 0x90: subtractRegister(Registers.B, 4); break;
            case 0x91: subtractRegister(Registers.C, 4); break;
            case 0x92: subtractRegister(Registers.D, 4); break;
            case 0x93: subtractRegister(Registers.E, 4); break;
            case 0x94: subtractRegister(Registers.H, 4); break;
            case 0x95: subtractRegister(Registers.L, 4); break;
            case 0x96: subtractValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xD6: subtractValue(immediateByte, 8); registers.PC++; break;
            // 4. SBC A,n - Subtract n + C flag from A
            case 0x9F: sbcRegister(Registers.A); cyclesToDelay += 4; break;
            case 0x98: sbcRegister(Registers.B); cyclesToDelay += 4; break;
            case 0x99: sbcRegister(Registers.C); cyclesToDelay += 4; break;
            case 0x9A: sbcRegister(Registers.D); cyclesToDelay += 4; break;
            case 0x9B: sbcRegister(Registers.E); cyclesToDelay += 4; break;
            case 0x9C: sbcRegister(Registers.H); cyclesToDelay += 4; break;
            case 0x9D: sbcRegister(Registers.L); cyclesToDelay += 4; break;
            case 0x9E: sbcValue(readRamAtRegs(Registers.H, Registers.L)); cyclesToDelay += 8; break;
            case 0xDE: sbcValue(immediateByte); registers.PC += 1; cyclesToDelay += 8; break;
            // 5. AND n - Logical AND n with register A, result in A
            case 0xA7: andRegister(Registers.A, 4); break;
            case 0xA0: andRegister(Registers.B, 4); break;
            case 0xA1: andRegister(Registers.C, 4); break;
            case 0xA2: andRegister(Registers.D, 4); break;
            case 0xA3: andRegister(Registers.E, 4); break;
            case 0xA4: andRegister(Registers.H, 4); break;
            case 0xA5: andRegister(Registers.L, 4); break;
            case 0xA6: andValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xE6: andValue(immediateByte, 8); registers.PC++; break;
            // 6. OR n - Logical OR n with register A, result in A
            case 0xB7: orRegister(Registers.A, 4); break;
            case 0xB0: orRegister(Registers.B, 4); break;
            case 0xB1: orRegister(Registers.C, 4); break;
            case 0xB2: orRegister(Registers.D, 4); break;
            case 0xB3: orRegister(Registers.E, 4); break;
            case 0xB4: orRegister(Registers.H, 4); break;
            case 0xB5: orRegister(Registers.L, 4); break;
            case 0xB6: orValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xF6: orValue(immediateByte, 8); registers.PC++; break;
            // 7. XOR n - Logical exclusive OR n with register A, result in A
            case 0xAF: xorRegister(Registers.A, 4); break;
            case 0xA8: xorRegister(Registers.B, 4); break;
            case 0xA9: xorRegister(Registers.C, 4); break;
            case 0xAA: xorRegister(Registers.D, 4); break;
            case 0xAB: xorRegister(Registers.E, 4); break;
            case 0xAC: xorRegister(Registers.H, 4); break;
            case 0xAD: xorRegister(Registers.L, 4); break;
            case 0xAE: xorValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xEE: xorValue(immediateByte, 8); registers.PC++; break;
            // 8. CP n - compare A with n, basically doing A - n and ignoring results, only doing this for the flag changes from the operation
            case 0xBF: cpARegister(Registers.A, 4); break;
            case 0xB8: cpARegister(Registers.B, 4); break;
            case 0xB9: cpARegister(Registers.C, 4); break;
            case 0xBA: cpARegister(Registers.D, 4); break;
            case 0xBB: cpARegister(Registers.E, 4); break;
            case 0xBC: cpARegister(Registers.H, 4); break;
            case 0xBD: cpARegister(Registers.L, 4); break;
            case 0xBE: cpAValue(readRamAtRegs(Registers.H, Registers.L), 8); break;
            case 0xFE: cpAValue(immediateByte, 8); registers.PC++; break;
            // 9. INC n - Increment register n
            case 0x3C: registers.write(Registers.A, inc8bit(registers.read(Registers.A))); cyclesToDelay += 4; break;
            case 0x04: registers.write(Registers.B, inc8bit(registers.read(Registers.B))); cyclesToDelay += 4; break;
            case 0x0C: registers.write(Registers.C, inc8bit(registers.read(Registers.C))); cyclesToDelay += 4; break;
            case 0x14: registers.write(Registers.D, inc8bit(registers.read(Registers.D))); cyclesToDelay += 4; break;
            case 0x1C: registers.write(Registers.E, inc8bit(registers.read(Registers.E))); cyclesToDelay += 4; break;
            case 0x24: registers.write(Registers.H, inc8bit(registers.read(Registers.H))); cyclesToDelay += 4; break;
            case 0x2C: registers.write(Registers.L, inc8bit(registers.read(Registers.L))); cyclesToDelay += 4; break;
            case 0x34: writeRamAtRegs(Registers.H, Registers.L, inc8bit(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay+=12; break;
            // 10. DEC n - Decrement register n
            case 0x3D: registers.write(Registers.A, dec8bit(registers.read(Registers.A))); cyclesToDelay+=4; break;
            case 0x05: registers.write(Registers.B, dec8bit(registers.read(Registers.B))); cyclesToDelay+=4; break;
            case 0x0D: registers.write(Registers.C, dec8bit(registers.read(Registers.C))); cyclesToDelay+=4; break;
            case 0x15: registers.write(Registers.D, dec8bit(registers.read(Registers.D))); cyclesToDelay+=4; break;
            case 0x1D: registers.write(Registers.E, dec8bit(registers.read(Registers.E))); cyclesToDelay+=4; break;
            case 0x25: registers.write(Registers.H, dec8bit(registers.read(Registers.H))); cyclesToDelay+=4; break;
            case 0x2D: registers.write(Registers.L, dec8bit(registers.read(Registers.L))); cyclesToDelay+=4; break;
            case 0x35: writeRamAtRegs(Registers.H, Registers.L, dec8bit(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay+=12; break;

        // 16-Bit Arithmetic    Pgs 90-93   4  Commands     ////////////////////////////////////////////////////////////
            // 1. ADD HL,n - Add n to HL
            case 0x09: addHL16bitRegisters(Registers.B, Registers.C); break;
            case 0x19: addHL16bitRegisters(Registers.D, Registers.E); break;
            case 0x29: addHL16bitRegisters(Registers.H, Registers.L); break;
            case 0x39: addHL16bitValue(registers.SP & 0xFFFF); break;
            // 2. ADD SP,n - Add immediate signed byte to SP
            case 0xE8: addSignedByteToSPWithFlagChanges(immediateByte); registers.PC += 1; cyclesToDelay += 16; break;
            // 3. INC nn - Increment register nn
            case 0x03: registers.incrementDouble(Registers.B, Registers.C); cyclesToDelay += 8; break;
            case 0x13: registers.incrementDouble(Registers.D, Registers.E); cyclesToDelay += 8; break;
            case 0x23: registers.incrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            case 0x33: registers.SP++; cyclesToDelay += 8; break;
            // 4. DEC nn - Decrement register nn
            case 0x0B: registers.decrementDouble(Registers.B, Registers.C); cyclesToDelay += 8; break;
            case 0x1B: registers.decrementDouble(Registers.D, Registers.E); cyclesToDelay += 8; break;
            case 0x2B: registers.decrementDouble(Registers.H, Registers.L); cyclesToDelay += 8; break;
            case 0x3B: registers.SP--; cyclesToDelay += 8; break;

        // Miscellaneous        Pgs 94-98   10 Commands     ////////////////////////////////////////////////////////////
            // 1 is CB opcode
            // 2. DAA - Decimal adjust register A
            case 0x27: daa(); cyclesToDelay += 4; break;
            // 3. CPL - Complement register A (i.e. flip all bits), set flags N and H
            case 0x2F: registers.write(Registers.A, ~registers.read(Registers.A)); registers.setFlagSubtract(); registers.setFlagHalfCarry(); cyclesToDelay += 4; break;
            // 4. CCF - Complement C flag (i.e. flip it)
            case 0x3F: if (registers.flagCarry()) registers.resetFlagCarry(); else registers.setFlagCarry(); registers.resetFlagZero(); registers.resetFlagHalfCarry(); cyclesToDelay += 4; break;
            // 5. SCF - Set C flag
            case 0x37: registers.setFlagCarry(); cyclesToDelay += 4; break;
            // 6. NOP - No operation
            case 0x00: cyclesToDelay += 4; break;
            // 7. HALT - Stops just the CPU running until an interrupt occurs. If interrupts are disable via IME, then do not halt the Game Boy (in this case, following byte get reads twice next tick)
            case 0x76: if (registers.IME) cpuHalted = true; else failedHalt = true; cyclesToDelay += 4; break;
            // 8. STOP - Stop CPU and LCD from doing anything until a button is pressed
            case 0x10: cpuAndLcdStopped = true; cyclesToDelay += 4; break;
            // 9. DI - Disable interrupts (via IME, interrupt master enable flag)
            // Note: whilst IME is changed, interrupts are only no longer acted on after the opcode after this one has been executed
            case 0xF3: registers.IME = false; imeChangeDelayed = true; cyclesToDelay += 4; break;
            // 10. EI - Enable interrupts (via IME, same delay as above opcode)
            // Note: whilst IME is changed, interrupts are only actually acted on after the opcode after this one has been executed
            case 0xFB: registers.IME = true; imeChangeDelayed = true; cyclesToDelay += 4; break;

        // Rotates & Shifts     Pgs 99-107  11 Commands     ////////////////////////////////////////////////////////////
            // 1. RLCA - Rotate A left, carry flag = old bit 7
            case 0x07: registers.write(Registers.A, rlcN(registers.read(Registers.A))); cyclesToDelay += 4; break;
            // 2. RLA - Rotate A left through carry flag (duplicate of RL A implemented further down as a CB opcode)
            case 0x17: registers.write(Registers.A, rlN(registers.read(Registers.A))); cyclesToDelay += 4; break;
            // 3. RRCA - Rotate A right, old bit 0 to carry flag (duplicate of RRC A implemented further down as a CB opcode)
            case 0x0F: registers.write(Registers.A, rrcN(registers.read(Registers.A))); cyclesToDelay += 4; break;
            // 4. RRA - Rotate A right through carry flag (duplicate of RR A implemented further down as a CB opcode)
            case 0x1F: registers.write(Registers.A, rrN(registers.read(Registers.A))); cyclesToDelay += 4; break;
            // 5 - 11 are CB opcodes

        // Bit Opcodes          Pgs 108-110 3  Commands     ////////////////////////////////////////////////////////////
            /*
                These are all CB opcodes
             */

        // Jumps                Pgs 111-113 5  Commands     ////////////////////////////////////////////////////////////
            // 1. JP nn - Jump to address nn
            case 0xC3: registers.PC = immediateTwoBytesSwapped; cyclesToDelay += 12; break;
            // 2. JP cc,nn - Jump to address nn if condition is true
            case 0xC2: registers.PC += 2; if (!registers.flagZero()) registers.PC = immediateTwoBytesSwapped; cyclesToDelay += 12; break;
            case 0xCA: registers.PC += 2; if (registers.flagZero()) registers.PC = immediateTwoBytesSwapped; cyclesToDelay += 12; break;
            case 0xD2: registers.PC += 2; if (!registers.flagCarry()) registers.PC = immediateTwoBytesSwapped; cyclesToDelay += 12; break;
            case 0xDA: registers.PC += 2; if (registers.flagCarry()) registers.PC = immediateTwoBytesSwapped; cyclesToDelay += 12; break;
            // 3. JP (HL) - Jump to address in HL (set PC to HL)
            case 0xE9: registers.PC = registers.readTwo(Registers.H, Registers.L); cyclesToDelay += 4; break;
            // 4. JR n - Add n (one byte signed immediate value) to current address and jump to it
            case 0x18: registers.PC++; registers.PC += toByte(immediateByte); cyclesToDelay += 8; break;
            // 5. JR cc,m - If following condition is true, add n to current address then jump to that new address
            //           (reading n as a signed, 2's complement integer)
            // Note: incrementing PC for operand before action as action of the opcode affects the PC,
            // though really it doesn't matter too much as the same end result would be met, this just seems a bit cleaner
            case 0x20: registers.PC++; if(!registers.flagZero()) registers.PC += toByte(immediateByte); cyclesToDelay+=8; break;
            case 0x28: registers.PC++; if(registers.flagZero()) registers.PC += toByte(immediateByte); cyclesToDelay+=8; break;
            case 0x30: registers.PC++; if(!registers.flagCarry()) registers.PC += toByte(immediateByte); cyclesToDelay+=8; break;
            case 0x38: registers.PC++; if(registers.flagCarry()) registers.PC += toByte(immediateByte); cyclesToDelay+=8; break;

        // Calls                Pgs 114-115 2  Commands     ////////////////////////////////////////////////////////////
            // 1. CALL nn - Put address of next instruction on stack, then jump to address nn
            case 0xCD: callNn(immediateTwoBytesSwapped); cyclesToDelay += 12; break; // No need to inc PC for operands as that's handled in the method
            // 2. Call cc,nn - Call address n if condition is true
            case 0xC4: registers.PC += 2; if(!registers.flagZero()) callNn(immediateTwoBytesSwapped); cyclesToDelay += 12; break;
            case 0xCC: registers.PC += 2; if(registers.flagZero()) callNn(immediateTwoBytesSwapped); cyclesToDelay += 12; break;
            case 0xD4: registers.PC += 2; if(!registers.flagCarry()) callNn(immediateTwoBytesSwapped); cyclesToDelay += 12; break;
            case 0xDC: registers.PC += 2; if(registers.flagCarry()) callNn(immediateTwoBytesSwapped); cyclesToDelay += 12; break;

        // Restarts             Pg  116     1  Command      ////////////////////////////////////////////////////////////
            // 1. RST n - Push present address to stack, jump to address 0x0000 + n
            case 0xC7: pushTwoBytesToStack(registers.PC); registers.PC = 0x00; cyclesToDelay += 32; break;
            case 0xCF: pushTwoBytesToStack(registers.PC); registers.PC = 0x08; cyclesToDelay += 32; break;
            case 0xD7: pushTwoBytesToStack(registers.PC); registers.PC = 0x10; cyclesToDelay += 32; break;
            case 0xDF: pushTwoBytesToStack(registers.PC); registers.PC = 0x18; cyclesToDelay += 32; break;
            case 0xE7: pushTwoBytesToStack(registers.PC); registers.PC = 0x20; cyclesToDelay += 32; break;
            case 0xEF: pushTwoBytesToStack(registers.PC); registers.PC = 0x28; cyclesToDelay += 32; break;
            case 0xF7: pushTwoBytesToStack(registers.PC); registers.PC = 0x30; cyclesToDelay += 32; break;
            case 0xFF: pushTwoBytesToStack(registers.PC); registers.PC = 0x38; cyclesToDelay += 32; break;

        // Returns              Pgs 117-118 3  Commands     ////////////////////////////////////////////////////////////
            // 1. RET - Pop 2 bytes from stack & jump to that address
            case 0xC9: ret(); cyclesToDelay += 8; break;
            // 2. RET cc - Return if following condition is true
            case 0xC0: if (!registers.flagZero()) ret(); cyclesToDelay += 8; break;
            case 0xC8: if (registers.flagZero()) ret(); cyclesToDelay += 8; break;
            case 0xD0: if (!registers.flagCarry()) ret(); cyclesToDelay += 8; break;
            case 0xD8: if (registers.flagCarry()) ret(); cyclesToDelay += 8; break;
            // 3. RETI - Pop 2 bytes from stack, jump to that address, enable interrupts
            case 0xD9: ret(); registers.IME = true; cyclesToDelay += 8; break;


            case 0xCB:
                // Two byte opcodes with prefix CB, take those to another switch statement
                cbOpcodes();
                break;
            default:
                System.out.println("Unsupported opcode - " + minLengthHexString(2, opcode) + " (at address " + minLengthHexString(4, registers.PC-1) + ")");
                debugWarningPopup("Unsupported opcode - " + minLengthHexString(2, opcode)/* + "\n\n==================\n" + debugString*/);
                break;
        }
// @formatter:on

    }

    void cbOpcodes() {
        // Read second byte of the opcode
        int opcode = ram.readByte(registers.PC);

        registers.PC++;

// @formatter:off
        switch(opcode) {
        // Miscellaneous        Pgs 94-98   10 Commands     ////////////////////////////////////////////////////////////
            // 1. SWAP n - Swap upper and lower nibbles of n
            case 0x37: registers.write(Registers.A, swapN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x30: registers.write(Registers.B, swapN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x31: registers.write(Registers.C, swapN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x32: registers.write(Registers.D, swapN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x33: registers.write(Registers.E, swapN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x34: registers.write(Registers.H, swapN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x35: registers.write(Registers.L, swapN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x36: writeRamAtRegs(Registers.H, Registers.L, swapN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;

        // Rotates & Shifts     Pgs 99-107  11 Commands     ////////////////////////////////////////////////////////////
            // 1-4 aren't CB opcodes
            // 5. RLC n - Rotate n left, set C to value of old bit 7
            case 0x07: registers.write(Registers.A, rlcN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x00: registers.write(Registers.B, rlcN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x01: registers.write(Registers.C, rlcN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x02: registers.write(Registers.D, rlcN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x03: registers.write(Registers.E, rlcN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x04: registers.write(Registers.H, rlcN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x05: registers.write(Registers.L, rlcN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x06: writeRamAtRegs(Registers.H, Registers.L, rlcN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 6. RL n - Rotate n left through Carry flag
            case 0x17: registers.write(Registers.A, rlN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x10: registers.write(Registers.B, rlN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x11: registers.write(Registers.C, rlN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x12: registers.write(Registers.D, rlN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x13: registers.write(Registers.E, rlN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x14: registers.write(Registers.H, rlN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x15: registers.write(Registers.L, rlN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x16: writeRamAtRegs(Registers.H, Registers.L, rlN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 7. RRC n - Rotate n right, set C to old bit 0
            case 0x0F: registers.write(Registers.A, rrcN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x08: registers.write(Registers.B, rrcN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x09: registers.write(Registers.C, rrcN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x0A: registers.write(Registers.D, rrcN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x0B: registers.write(Registers.E, rrcN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x0C: registers.write(Registers.H, rrcN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x0D: registers.write(Registers.L, rrcN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x0E: writeRamAtRegs(Registers.H, Registers.L, rrcN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 8. RR n - Rotate n right through Carry flag
            case 0x1F: registers.write(Registers.A, rrN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x18: registers.write(Registers.B, rrN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x19: registers.write(Registers.C, rrN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x1A: registers.write(Registers.D, rrN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x1B: registers.write(Registers.E, rrN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x1C: registers.write(Registers.H, rrN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x1D: registers.write(Registers.L, rrN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x1E: writeRamAtRegs(Registers.H, Registers.L, rrN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 9. SLA n - Shift n left into Carry, set least significant bit of n to 0
            case 0x27: registers.write(Registers.A, slaN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x20: registers.write(Registers.B, slaN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x21: registers.write(Registers.C, slaN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x22: registers.write(Registers.D, slaN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x23: registers.write(Registers.E, slaN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x24: registers.write(Registers.H, slaN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x25: registers.write(Registers.L, slaN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x26: writeRamAtRegs(Registers.H, Registers.L, slaN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 10 SRA n - Shift n right into Carry, most significant bit doesn't change
            case 0x2F: registers.write(Registers.A, sraN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x28: registers.write(Registers.B, sraN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x29: registers.write(Registers.C, sraN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x2A: registers.write(Registers.D, sraN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x2B: registers.write(Registers.E, sraN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x2C: registers.write(Registers.H, sraN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x2D: registers.write(Registers.L, sraN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x2E: writeRamAtRegs(Registers.H, Registers.L, sraN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;
            // 11. SRL n - Shift n right into Carry, set most significant bit to 0
            case 0x3F: registers.write(Registers.A, srlN(registers.read(Registers.A))); cyclesToDelay += 8; break;
            case 0x38: registers.write(Registers.B, srlN(registers.read(Registers.B))); cyclesToDelay += 8; break;
            case 0x39: registers.write(Registers.C, srlN(registers.read(Registers.C))); cyclesToDelay += 8; break;
            case 0x3A: registers.write(Registers.D, srlN(registers.read(Registers.D))); cyclesToDelay += 8; break;
            case 0x3B: registers.write(Registers.E, srlN(registers.read(Registers.E))); cyclesToDelay += 8; break;
            case 0x3C: registers.write(Registers.H, srlN(registers.read(Registers.H))); cyclesToDelay += 8; break;
            case 0x3D: registers.write(Registers.L, srlN(registers.read(Registers.L))); cyclesToDelay += 8; break;
            case 0x3E: writeRamAtRegs(Registers.H, Registers.L, srlN(readRamAtRegs(Registers.H, Registers.L))); cyclesToDelay += 16; break;

        // Bit Opcodes          Pgs 108-110 3  Commands     ////////////////////////////////////////////////////////////
            // 1. BIT b,r - Test bit b in register r
            // Bit 0
            case 0x40: bitBR(registers.read(Registers.B), 0); cyclesToDelay += 8; break;
            case 0x41: bitBR(registers.read(Registers.C), 0); cyclesToDelay += 8; break;
            case 0x42: bitBR(registers.read(Registers.D), 0); cyclesToDelay += 8; break;
            case 0x43: bitBR(registers.read(Registers.E), 0); cyclesToDelay += 8; break;
            case 0x44: bitBR(registers.read(Registers.H), 0); cyclesToDelay += 8; break;
            case 0x45: bitBR(registers.read(Registers.L), 0); cyclesToDelay += 8; break;
            case 0x46: bitBR(readRamAtRegs(Registers.H, Registers.L), 0); cyclesToDelay += 16; break;
            case 0x47: bitBR(registers.read(Registers.A), 0); cyclesToDelay += 8; break;
            // Bit 1
            case 0x48: bitBR(registers.read(Registers.B), 1); cyclesToDelay += 8; break;
            case 0x49: bitBR(registers.read(Registers.C), 1); cyclesToDelay += 8; break;
            case 0x4A: bitBR(registers.read(Registers.D), 1); cyclesToDelay += 8; break;
            case 0x4B: bitBR(registers.read(Registers.E), 1); cyclesToDelay += 8; break;
            case 0x4C: bitBR(registers.read(Registers.H), 1); cyclesToDelay += 8; break;
            case 0x4D: bitBR(registers.read(Registers.L), 1); cyclesToDelay += 8; break;
            case 0x4E: bitBR(readRamAtRegs(Registers.H, Registers.L), 1); cyclesToDelay += 16; break;
            case 0x4F: bitBR(registers.read(Registers.A), 1); cyclesToDelay += 8; break;
            // Bit 2
            case 0x50: bitBR(registers.read(Registers.B), 2); cyclesToDelay += 8; break;
            case 0x51: bitBR(registers.read(Registers.C), 2); cyclesToDelay += 8; break;
            case 0x52: bitBR(registers.read(Registers.D), 2); cyclesToDelay += 8; break;
            case 0x53: bitBR(registers.read(Registers.E), 2); cyclesToDelay += 8; break;
            case 0x54: bitBR(registers.read(Registers.H), 2); cyclesToDelay += 8; break;
            case 0x55: bitBR(registers.read(Registers.L), 2); cyclesToDelay += 8; break;
            case 0x56: bitBR(readRamAtRegs(Registers.H, Registers.L), 2); cyclesToDelay += 16; break;
            case 0x57: bitBR(registers.read(Registers.A), 2); cyclesToDelay += 8; break;
            // Bit 3
            case 0x58: bitBR(registers.read(Registers.B), 3); cyclesToDelay += 8; break;
            case 0x59: bitBR(registers.read(Registers.C), 3); cyclesToDelay += 8; break;
            case 0x5A: bitBR(registers.read(Registers.D), 3); cyclesToDelay += 8; break;
            case 0x5B: bitBR(registers.read(Registers.E), 3); cyclesToDelay += 8; break;
            case 0x5C: bitBR(registers.read(Registers.H), 3); cyclesToDelay += 8; break;
            case 0x5D: bitBR(registers.read(Registers.L), 3); cyclesToDelay += 8; break;
            case 0x5E: bitBR(readRamAtRegs(Registers.H, Registers.L), 3); cyclesToDelay += 16; break;
            case 0x5F: bitBR(registers.read(Registers.A), 3); cyclesToDelay += 8; break;
            // Bit 4
            case 0x60: bitBR(registers.read(Registers.B), 4); cyclesToDelay += 8; break;
            case 0x61: bitBR(registers.read(Registers.C), 4); cyclesToDelay += 8; break;
            case 0x62: bitBR(registers.read(Registers.D), 4); cyclesToDelay += 8; break;
            case 0x63: bitBR(registers.read(Registers.E), 4); cyclesToDelay += 8; break;
            case 0x64: bitBR(registers.read(Registers.H), 4); cyclesToDelay += 8; break;
            case 0x65: bitBR(registers.read(Registers.L), 4); cyclesToDelay += 8; break;
            case 0x66: bitBR(readRamAtRegs(Registers.H, Registers.L), 4); cyclesToDelay += 16; break;
            case 0x67: bitBR(registers.read(Registers.A), 4); cyclesToDelay += 8; break;
            // Bit 5
            case 0x68: bitBR(registers.read(Registers.B), 5); cyclesToDelay += 8; break;
            case 0x69: bitBR(registers.read(Registers.C), 5); cyclesToDelay += 8; break;
            case 0x6A: bitBR(registers.read(Registers.D), 5); cyclesToDelay += 8; break;
            case 0x6B: bitBR(registers.read(Registers.E), 5); cyclesToDelay += 8; break;
            case 0x6C: bitBR(registers.read(Registers.H), 5); cyclesToDelay += 8; break;
            case 0x6D: bitBR(registers.read(Registers.L), 5); cyclesToDelay += 8; break;
            case 0x6E: bitBR(readRamAtRegs(Registers.H, Registers.L), 5); cyclesToDelay += 16; break;
            case 0x6F: bitBR(registers.read(Registers.A), 5); cyclesToDelay += 8; break;
            // Bit 6
            case 0x70: bitBR(registers.read(Registers.B), 6); cyclesToDelay += 8; break;
            case 0x71: bitBR(registers.read(Registers.C), 6); cyclesToDelay += 8; break;
            case 0x72: bitBR(registers.read(Registers.D), 6); cyclesToDelay += 8; break;
            case 0x73: bitBR(registers.read(Registers.E), 6); cyclesToDelay += 8; break;
            case 0x74: bitBR(registers.read(Registers.H), 6); cyclesToDelay += 8; break;
            case 0x75: bitBR(registers.read(Registers.L), 6); cyclesToDelay += 8; break;
            case 0x76: bitBR(readRamAtRegs(Registers.H, Registers.L), 6); cyclesToDelay += 16; break;
            case 0x77: bitBR(registers.read(Registers.A), 6); cyclesToDelay += 8; break;
            // Bit 7
            case 0x78: bitBR(registers.read(Registers.B), 7); cyclesToDelay += 8; break;
            case 0x79: bitBR(registers.read(Registers.C), 7); cyclesToDelay += 8; break;
            case 0x7A: bitBR(registers.read(Registers.D), 7); cyclesToDelay += 8; break;
            case 0x7B: bitBR(registers.read(Registers.E), 7); cyclesToDelay += 8; break;
            case 0x7C: bitBR(registers.read(Registers.H), 7); cyclesToDelay += 8; break;
            case 0x7D: bitBR(registers.read(Registers.L), 7); cyclesToDelay += 8; break;
            case 0x7E: bitBR(readRamAtRegs(Registers.H, Registers.L), 7); cyclesToDelay += 16; break;
            case 0x7F: bitBR(registers.read(Registers.A), 7); cyclesToDelay += 8; break;
            // 2. SET b,r - Reset bit b in register r
            // Bit 0
            case 0xC0: registers.setRegisterBit(Registers.B, 0, 1); cyclesToDelay += 8; break;
            case 0xC1: registers.setRegisterBit(Registers.C, 0, 1); cyclesToDelay += 8; break;
            case 0xC2: registers.setRegisterBit(Registers.D, 0, 1); cyclesToDelay += 8; break;
            case 0xC3: registers.setRegisterBit(Registers.E, 0, 1); cyclesToDelay += 8; break;
            case 0xC4: registers.setRegisterBit(Registers.H, 0, 1); cyclesToDelay += 8; break;
            case 0xC5: registers.setRegisterBit(Registers.L, 0, 1); cyclesToDelay += 8; break;
            case 0xC6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 0, 1); cyclesToDelay += 16; break;
            case 0xC7: registers.setRegisterBit(Registers.A, 0, 1); cyclesToDelay += 8; break;
            // Bit 1
            case 0xC8: registers.setRegisterBit(Registers.B, 1, 1); cyclesToDelay += 8; break;
            case 0xC9: registers.setRegisterBit(Registers.C, 1, 1); cyclesToDelay += 8; break;
            case 0xCA: registers.setRegisterBit(Registers.D, 1, 1); cyclesToDelay += 8; break;
            case 0xCB: registers.setRegisterBit(Registers.E, 1, 1); cyclesToDelay += 8; break;
            case 0xCC: registers.setRegisterBit(Registers.H, 1, 1); cyclesToDelay += 8; break;
            case 0xCD: registers.setRegisterBit(Registers.L, 1, 1); cyclesToDelay += 8; break;
            case 0xCE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 1, 1); cyclesToDelay += 16; break;
            case 0xCF: registers.setRegisterBit(Registers.A, 1, 1); cyclesToDelay += 8; break;
            // Bit 2
            case 0xD0: registers.setRegisterBit(Registers.B, 2, 1); cyclesToDelay += 8; break;
            case 0xD1: registers.setRegisterBit(Registers.C, 2, 1); cyclesToDelay += 8; break;
            case 0xD2: registers.setRegisterBit(Registers.D, 2, 1); cyclesToDelay += 8; break;
            case 0xD3: registers.setRegisterBit(Registers.E, 2, 1); cyclesToDelay += 8; break;
            case 0xD4: registers.setRegisterBit(Registers.H, 2, 1); cyclesToDelay += 8; break;
            case 0xD5: registers.setRegisterBit(Registers.L, 2, 1); cyclesToDelay += 8; break;
            case 0xD6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 2, 1); cyclesToDelay += 16; break;
            case 0xD7: registers.setRegisterBit(Registers.A, 2, 1); cyclesToDelay += 8; break;
            // Bit 3
            case 0xD8: registers.setRegisterBit(Registers.B, 3, 1); cyclesToDelay += 8; break;
            case 0xD9: registers.setRegisterBit(Registers.C, 3, 1); cyclesToDelay += 8; break;
            case 0xDA: registers.setRegisterBit(Registers.D, 3, 1); cyclesToDelay += 8; break;
            case 0xDB: registers.setRegisterBit(Registers.E, 3, 1); cyclesToDelay += 8; break;
            case 0xDC: registers.setRegisterBit(Registers.H, 3, 1); cyclesToDelay += 8; break;
            case 0xDD: registers.setRegisterBit(Registers.L, 3, 1); cyclesToDelay += 8; break;
            case 0xDE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 3, 1); cyclesToDelay += 16; break;
            case 0xDF: registers.setRegisterBit(Registers.A, 3, 1); cyclesToDelay += 8; break;
            // Bit 4
            case 0xE0: registers.setRegisterBit(Registers.B, 4, 1); cyclesToDelay += 8; break;
            case 0xE1: registers.setRegisterBit(Registers.C, 4, 1); cyclesToDelay += 8; break;
            case 0xE2: registers.setRegisterBit(Registers.D, 4, 1); cyclesToDelay += 8; break;
            case 0xE3: registers.setRegisterBit(Registers.E, 4, 1); cyclesToDelay += 8; break;
            case 0xE4: registers.setRegisterBit(Registers.H, 4, 1); cyclesToDelay += 8; break;
            case 0xE5: registers.setRegisterBit(Registers.L, 4, 1); cyclesToDelay += 8; break;
            case 0xE6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 4, 1); cyclesToDelay += 16; break;
            case 0xE7: registers.setRegisterBit(Registers.A, 4, 1); cyclesToDelay += 8; break;
            // Bit 5
            case 0xE8: registers.setRegisterBit(Registers.B, 5, 1); cyclesToDelay += 8; break;
            case 0xE9: registers.setRegisterBit(Registers.C, 5, 1); cyclesToDelay += 8; break;
            case 0xEA: registers.setRegisterBit(Registers.D, 5, 1); cyclesToDelay += 8; break;
            case 0xEB: registers.setRegisterBit(Registers.E, 5, 1); cyclesToDelay += 8; break;
            case 0xEC: registers.setRegisterBit(Registers.H, 5, 1); cyclesToDelay += 8; break;
            case 0xED: registers.setRegisterBit(Registers.L, 5, 1); cyclesToDelay += 8; break;
            case 0xEE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 5, 1); cyclesToDelay += 16; break;
            case 0xEF: registers.setRegisterBit(Registers.A, 5, 1); cyclesToDelay += 8; break;
            // Bit 6
            case 0xF0: registers.setRegisterBit(Registers.B, 6, 1); cyclesToDelay += 8; break;
            case 0xF1: registers.setRegisterBit(Registers.C, 6, 1); cyclesToDelay += 8; break;
            case 0xF2: registers.setRegisterBit(Registers.D, 6, 1); cyclesToDelay += 8; break;
            case 0xF3: registers.setRegisterBit(Registers.E, 6, 1); cyclesToDelay += 8; break;
            case 0xF4: registers.setRegisterBit(Registers.H, 6, 1); cyclesToDelay += 8; break;
            case 0xF5: registers.setRegisterBit(Registers.L, 6, 1); cyclesToDelay += 8; break;
            case 0xF6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 6, 1); cyclesToDelay += 16; break;
            case 0xF7: registers.setRegisterBit(Registers.A, 6, 1); cyclesToDelay += 8; break;
            // Bit 7
            case 0xF8: registers.setRegisterBit(Registers.B, 7, 1); cyclesToDelay += 8; break;
            case 0xF9: registers.setRegisterBit(Registers.C, 7, 1); cyclesToDelay += 8; break;
            case 0xFA: registers.setRegisterBit(Registers.D, 7, 1); cyclesToDelay += 8; break;
            case 0xFB: registers.setRegisterBit(Registers.E, 7, 1); cyclesToDelay += 8; break;
            case 0xFC: registers.setRegisterBit(Registers.H, 7, 1); cyclesToDelay += 8; break;
            case 0xFD: registers.setRegisterBit(Registers.L, 7, 1); cyclesToDelay += 8; break;
            case 0xFE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 7, 1); cyclesToDelay += 16; break;
            case 0xFF: registers.setRegisterBit(Registers.A, 7, 1); cyclesToDelay += 8; break;
            // 3. RES b,r - Reset bit b in register r
            // Bit 0
            case 0x80: registers.setRegisterBit(Registers.B, 0, 0); cyclesToDelay += 8; break;
            case 0x81: registers.setRegisterBit(Registers.C, 0, 0); cyclesToDelay += 8; break;
            case 0x82: registers.setRegisterBit(Registers.D, 0, 0); cyclesToDelay += 8; break;
            case 0x83: registers.setRegisterBit(Registers.E, 0, 0); cyclesToDelay += 8; break;
            case 0x84: registers.setRegisterBit(Registers.H, 0, 0); cyclesToDelay += 8; break;
            case 0x85: registers.setRegisterBit(Registers.L, 0, 0); cyclesToDelay += 8; break;
            case 0x86: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 0, 0); cyclesToDelay += 16; break;
            case 0x87: registers.setRegisterBit(Registers.A, 0, 0); cyclesToDelay += 8; break;
            // Bit 1
            case 0x88: registers.setRegisterBit(Registers.B, 1, 0); cyclesToDelay += 8; break;
            case 0x89: registers.setRegisterBit(Registers.C, 1, 0); cyclesToDelay += 8; break;
            case 0x8A: registers.setRegisterBit(Registers.D, 1, 0); cyclesToDelay += 8; break;
            case 0x8B: registers.setRegisterBit(Registers.E, 1, 0); cyclesToDelay += 8; break;
            case 0x8C: registers.setRegisterBit(Registers.H, 1, 0); cyclesToDelay += 8; break;
            case 0x8D: registers.setRegisterBit(Registers.L, 1, 0); cyclesToDelay += 8; break;
            case 0x8E: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 1, 0); cyclesToDelay += 16; break;
            case 0x8F: registers.setRegisterBit(Registers.A, 1, 0); cyclesToDelay += 8; break;
            // Bit 2
            case 0x90: registers.setRegisterBit(Registers.B, 2, 0); cyclesToDelay += 8; break;
            case 0x91: registers.setRegisterBit(Registers.C, 2, 0); cyclesToDelay += 8; break;
            case 0x92: registers.setRegisterBit(Registers.D, 2, 0); cyclesToDelay += 8; break;
            case 0x93: registers.setRegisterBit(Registers.E, 2, 0); cyclesToDelay += 8; break;
            case 0x94: registers.setRegisterBit(Registers.H, 2, 0); cyclesToDelay += 8; break;
            case 0x95: registers.setRegisterBit(Registers.L, 2, 0); cyclesToDelay += 8; break;
            case 0x96: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 2, 0); cyclesToDelay += 16; break;
            case 0x97: registers.setRegisterBit(Registers.A, 2, 0); cyclesToDelay += 8; break;
            // Bit 3
            case 0x98: registers.setRegisterBit(Registers.B, 3, 0); cyclesToDelay += 8; break;
            case 0x99: registers.setRegisterBit(Registers.C, 3, 0); cyclesToDelay += 8; break;
            case 0x9A: registers.setRegisterBit(Registers.D, 3, 0); cyclesToDelay += 8; break;
            case 0x9B: registers.setRegisterBit(Registers.E, 3, 0); cyclesToDelay += 8; break;
            case 0x9C: registers.setRegisterBit(Registers.H, 3, 0); cyclesToDelay += 8; break;
            case 0x9D: registers.setRegisterBit(Registers.L, 3, 0); cyclesToDelay += 8; break;
            case 0x9E: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 3, 0); cyclesToDelay += 16; break;
            case 0x9F: registers.setRegisterBit(Registers.A, 3, 0); cyclesToDelay += 8; break;
            // Bit 4
            case 0xA0: registers.setRegisterBit(Registers.B, 4, 0); cyclesToDelay += 8; break;
            case 0xA1: registers.setRegisterBit(Registers.C, 4, 0); cyclesToDelay += 8; break;
            case 0xA2: registers.setRegisterBit(Registers.D, 4, 0); cyclesToDelay += 8; break;
            case 0xA3: registers.setRegisterBit(Registers.E, 4, 0); cyclesToDelay += 8; break;
            case 0xA4: registers.setRegisterBit(Registers.H, 4, 0); cyclesToDelay += 8; break;
            case 0xA5: registers.setRegisterBit(Registers.L, 4, 0); cyclesToDelay += 8; break;
            case 0xA6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 4, 0); cyclesToDelay += 16; break;
            case 0xA7: registers.setRegisterBit(Registers.A, 4, 0); cyclesToDelay += 8; break;
            // Bit 5
            case 0xA8: registers.setRegisterBit(Registers.B, 5, 0); cyclesToDelay += 8; break;
            case 0xA9: registers.setRegisterBit(Registers.C, 5, 0); cyclesToDelay += 8; break;
            case 0xAA: registers.setRegisterBit(Registers.D, 5, 0); cyclesToDelay += 8; break;
            case 0xAB: registers.setRegisterBit(Registers.E, 5, 0); cyclesToDelay += 8; break;
            case 0xAC: registers.setRegisterBit(Registers.H, 5, 0); cyclesToDelay += 8; break;
            case 0xAD: registers.setRegisterBit(Registers.L, 5, 0); cyclesToDelay += 8; break;
            case 0xAE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 5, 0); cyclesToDelay += 16; break;
            case 0xAF: registers.setRegisterBit(Registers.A, 5, 0); cyclesToDelay += 8; break;
            // Bit 6
            case 0xB0: registers.setRegisterBit(Registers.B, 6, 0); cyclesToDelay += 8; break;
            case 0xB1: registers.setRegisterBit(Registers.C, 6, 0); cyclesToDelay += 8; break;
            case 0xB2: registers.setRegisterBit(Registers.D, 6, 0); cyclesToDelay += 8; break;
            case 0xB3: registers.setRegisterBit(Registers.E, 6, 0); cyclesToDelay += 8; break;
            case 0xB4: registers.setRegisterBit(Registers.H, 6, 0); cyclesToDelay += 8; break;
            case 0xB5: registers.setRegisterBit(Registers.L, 6, 0); cyclesToDelay += 8; break;
            case 0xB6: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 6, 0); cyclesToDelay += 16; break;
            case 0xB7: registers.setRegisterBit(Registers.A, 6, 0); cyclesToDelay += 8; break;
            // Bit 7
            case 0xB8: registers.setRegisterBit(Registers.B, 7, 0); cyclesToDelay += 8; break;
            case 0xB9: registers.setRegisterBit(Registers.C, 7, 0); cyclesToDelay += 8; break;
            case 0xBA: registers.setRegisterBit(Registers.D, 7, 0); cyclesToDelay += 8; break;
            case 0xBB: registers.setRegisterBit(Registers.E, 7, 0); cyclesToDelay += 8; break;
            case 0xBC: registers.setRegisterBit(Registers.H, 7, 0); cyclesToDelay += 8; break;
            case 0xBD: registers.setRegisterBit(Registers.L, 7, 0); cyclesToDelay += 8; break;
            case 0xBE: setBitAtMemoryAddress(registers.readTwo(Registers.H, Registers.L), 7, 0); cyclesToDelay += 16; break;
            case 0xBF: registers.setRegisterBit(Registers.A, 7, 0); cyclesToDelay += 8; break;
            default:
                System.out.println("Unsupported opcode - CB " + minLengthHexString(2, opcode) + "\tPC " + minLengthHexString(4, registers.PC-1));
                debugWarningPopup("Unsupported opcode - CB " + minLengthHexString(2, opcode) + "\n\nPC " + minLengthHexString(4, registers.PC-1));
                break;
        }
// @formatter:on
    }

    void debugWarningPopup(String warningMessage) {
        int result = JOptionPane.showConfirmDialog(null, warningMessage + "\n\nContinue running?", ImportantStrings.MAINWINDOWTITLE, JOptionPane.INFORMATION_MESSAGE);
        if (result != JOptionPane.YES_OPTION) {
            stop();
        }
    }

    // Helper methods to make implementing opcodes easier

    public static String minLengthHexString(int minLength, int val) {
        String shortStr = Integer.toHexString(val).toUpperCase();
        if (shortStr.length() == minLength)
            return "0x" + shortStr;

        int paddingNeeded = minLength - shortStr.length();
        String finalStr = "";
        for (int i=0; i<paddingNeeded; i++)
            finalStr += "0";

        return "0x" + finalStr + shortStr;
    }

    int swapTwoBytes(int originalBytes) {
        return (originalBytes & 0xFF) << 8 | ((originalBytes & 0xFF00) >> 8);
    }

    int swapNibblesInByte(int originalByte) {
        return (originalByte & 0x0F) << 4 | ((originalByte & 0xF0) >> 4);
    }

    int readRamAtRegs(int registerA, int registerB) {
        // Shortens the process of reading a byte from memory where the memory location is held in 2 defined registers
        return ram.readByte(registers.readTwo(registerA, registerB));
    }

    void writeRamAtRegs(int registerA, int registerB, int value) {
        ram.writeByte(registers.readTwo(registerA, registerB), value);
    }

    // Note: if used to convert an int where the original binary was supposed to be 2's complement,
    //       then this code can treat the returned value as that int's true value (i.e. what the 2's complement represented)
    byte toByte(int val) {
        return (byte) (val & 0xFF);
    }

    void pushTwoBytesToStack(int bytes) {
        // Significance of "push" being that the two bytes are put onto the stack and the stack counter is adjusted accordingly
        registers.SP--;
        ram.writeByte(registers.SP, (bytes >> 8) & 0xFF);
        registers.SP--;
        ram.writeByte(registers.SP, bytes & 0xFF);
    }

    int popTwoBytesFromStack() {
        // Significance of "pop" being two bytes are read from the stack and the stack counter is adjusted accordingly
        int byte1 = ram.readByte(registers.SP);
        registers.SP++;
        int byte2 = ram.readByte(registers.SP);
        registers.SP++;
        return ((byte2 << 8) | byte1) & 0xFFFF;
    }

    void setBitAtMemoryAddress(int address, int bit, int val) {
        int mask = 0b1 << bit;
        int initialValue = ram.readByte(address);

        int result = 0;
        if (val == 0)
            result = initialValue & ~mask;
        else
            result = initialValue | mask;

        ram.writeByte(address, result);
    }

    // Methods actually implementing opcodes

    void addSignedByteToSPWithFlagChanges(int value) {
        int signedValue = toByte(value);
        int result = (registers.SP + signedValue) & 0xFFFF;
        registers.writeTwo(Registers.H, Registers.L, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set H if necessary
        if (((registers.SP & 0x0F) + (value & 0x0F)) > 0x0F)
            registers.setFlagHalfCarry();
        // Set C if necessary
        if (((registers.SP + signedValue) & 0xFFFF0000) != 0)
            registers.setFlagCarry();

        // NOTE: Really unsure about these flags, but can't seem to work out exactly how they work
    }

    void addRegister(int registerFlag, int cycles) {
        addValue(registers.read(registerFlag), cycles);
    }
    void addValue(int value, int cycles) {
        int registerAValue = registers.read(Registers.A);
        int result = (registerAValue + value) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set H if carry from bit 3
        int halfAddResult = (registerAValue & 0x0F) + (value & 0x0F);
        if (halfAddResult > 0xF)
            registers.setFlagZero();
        // Set C if carry from bit 7
        if (value + registerAValue > 0xFF)
            registers.setFlagCarry();

        cyclesToDelay += cycles;
    }

    void adcRegister(int registerFlag) { adcValue(registers.read(registerFlag)); }
    void adcValue(int value) {
        int registerAValue = registers.read(Registers.A);
        int carryFlag = registers.flagCarry() ? 1 : 0;

        int result = (registerAValue + carryFlag + value) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set H if carry from bit 3
        if ((value & 0x0F) + carryFlag + (registerAValue & 0x0F) > 0x0F)
            registers.setFlagHalfCarry();
        // Set C if carry from bit 7
        if (registerAValue + carryFlag + value > 0xFF)
            registers.setFlagCarry();
    }

    void sbcRegister(int registerFlag) { sbcValue(registers.read(registerFlag)); }
    void sbcValue(int value) {
        int registerAValue = registers.read(Registers.A);
        int carryFlag = registers.flagCarry() ? 1 : 0;

        int result = (registerAValue - carryFlag - value) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set H if no borrow form bit 4
        if (((value + carryFlag) & 0x0F) > (registerAValue & 0x0F))
            registers.setFlagHalfCarry();
        // Set C if no borrow
        if (value + carryFlag > registerAValue)
            registers.setFlagCarry();
    }

    void subtractRegister(int registerFlag, int cycles) { subtractValue(registers.read(registerFlag), cycles); }
    void subtractValue(int value, int cycles) {
        int registerAValue = registers.read(Registers.A);
        int result = (registerAValue - value) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set N
        registers.setFlagSubtract();
        // Set H if no borrow from bit 4
        if ((value & 0x0F) > (registerAValue & 0x0F))
            registers.setFlagHalfCarry();
        // Set C for no borrow ( A < n )
        if (value > registerAValue)
            registers.setFlagCarry();

        cyclesToDelay += cycles;
    }

    void andRegister(int registerFlag, int cycles) {
        andValue(registers.read(registerFlag), cycles);
    }
    void andValue (int value, int cycles) {
        int registerAValue = registers.read(Registers.A);
        int result = (value & registerAValue) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set H
        registers.setFlagHalfCarry();

        cyclesToDelay += cycles;
    }

    void orRegister(int registerFlag, int cycles) {
        orValue(registers.read(registerFlag), cycles);
    }
    void orValue(int value, int cycles) {
        int registerAValue = registers.read(Registers.A);
        int result = (value | registerAValue) & 0xFF;
        registers.write(Registers.A, result);

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result  == 0)
            registers.setFlagZero();

        cyclesToDelay += cycles;
    }

    void xorRegister(int registerFlag, int cycles) {
        xorValue(registers.read(registerFlag), cycles);
    }
    void xorValue(int value, int cycles) {
        int registerAValue = registers.read(Registers.A);
        int result = (value ^ registerAValue) & 0xFF;
        registers.write(Registers.A, result);

        // If result is zero, set Z flag. Reset all other flags
        registers.resetAllFlags();
        if (result == 0)
            registers.setFlagZero();

        cyclesToDelay += cycles;
    }

    int inc8bit(int value) {
        int newVal = (value + 1) & 0xFF;

        // Update relevant flags
        // Set Z if result is 0
        if (newVal == 0x00)
            registers.setFlagZero();
        else
            registers.resetFlagZero();
        // Reset N
        registers.resetFlagSubtract();
        // Set H if carry from bit 3
        if ((value & 0x0F) == 0x0F)
            registers.setFlagHalfCarry();
        else
            registers.resetFlagHalfCarry();

        return newVal;
    }

    int dec8bit(int value) {
        int newVal = (value - 1) & 0xFF;

        // Update relevant flags
        // Set Z if result is 0
        if (newVal == 0x00)
            registers.setFlagZero();
        else
            registers.resetFlagZero();
        // Set N
        registers.setFlagSubtract();
        // set H if no borrow from bit 4
        if ((value & 0x0F) == 0x00)
            registers.setFlagHalfCarry();
        else
            registers.resetFlagHalfCarry();

        return newVal;
    }

    void cpARegister(int registerFlag, int cycles) {
        cpAValue(registers.read(registerFlag), cycles);
    }
    void cpAValue(int value, int cycles) {
        // Basically simulate A - value, not actually bothering to do it since really we're only interested in the flags
        int registerAValue = registers.read(Registers.A);

        // All flags are affected by this operation, so reset them all to zero first
        registers.resetAllFlags();

        // Set Z if result is zero (A == value)
        if (value == registerAValue)
            registers.setFlagZero();

        // Set N
        registers.setFlagSubtract();

        // Set H if no borrow from bit 4
        if ((value & 0x0F) > (registerAValue & 0x0F))
            registers.setFlagHalfCarry();

        // Set C for no borrow ( A < n )
        if (value > registerAValue)
            registers.setFlagCarry();

        cyclesToDelay += cycles;
    }

    void addHL16bitRegisters(int register1, int register2) {
        addHL16bitValue(registers.readTwo(register1, register2));
    }
    void addHL16bitValue(int nValue) {
        int hlValue = registers.readTwo(Registers.H, Registers.L);
        int result = hlValue + (nValue & 0xFFFF);
        registers.writeTwo(Registers.H, Registers.L, result);

        // Reset N
        registers.resetFlagSubtract();
        // Set H if carry from bit 11
        int maxVal12Bits = 0x0FFF;
        if (result > maxVal12Bits)
            registers.setFlagHalfCarry();
        // Set C if carry from bit 15
        int maxVal16bits = 0xFFFF;
        if (result > maxVal16bits)
            registers.setFlagCarry();

        cyclesToDelay += 8;
    }

    int swapN(int orignalByte) {
        int result = swapNibblesInByte(orignalByte) & 0xFF;

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();

        return result;
    }

    void daa() {
        int value = registers.read(Registers.A);

        if (!registers.flagSubtract()) {
            if (registers.flagHalfCarry() || (value & 0x0F) > 0x09)
                value += 0x06;
            if (registers.flagCarry() || (value & 0xF0) > 0x90)
                value += 0x60;
        } else {
            if (registers.flagHalfCarry())
                value = (value - 0x06);// & 0xFF;
            if (registers.flagCarry())
                value = (value - 0x60);// & 0xFF;
        }

        registers.write(Registers.A, value & 0xFF);

        // Set Z if result is zero
        if (value == 0)
            registers.setFlagZero();
        else
            registers.resetFlagZero();
        // Reset H
        registers.resetFlagHalfCarry();
        // Set C if carry from bit 7
        if ((value & 0x100) != 0)
            registers.setFlagCarry();
        else
            registers.resetFlagCarry();
    }

    int rlcN(int originalByte) {
        // Rotate byte left 1 and make carry flag value of old bit 7
        int result = (originalByte << 1) & 0xFF;
        result = (originalByte & 0x80) != 0 ? result | 0x01 : result & ~0x01;

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set C to old value of bit 7
        if ((originalByte & 0b10000000) != 0)
            registers.setFlagCarry();
        else
            registers.resetFlagCarry();

        return result;
    }

    int rlN(int originalByte) {
        // Shift byte to the left 1 and make bit 0 value of carry flag
        int result = (originalByte << 1) & 0xFF;
        result = registers.flagCarry() ? result | 0x01 : result & ~0x01;

        // Reset all flags
        registers.resetAllFlags();
        // Carry flag contains original bit 7 data
        if ((originalByte & 0x80) != 0)
            registers.setFlagCarry();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();

        return result;
    }

    int rrcN(int originalByte) {
        // Rotate byte to the right 1
        int result = (originalByte >> 1) & 0xFF;
        result = (originalByte & 0x80) != 0 ? result | 0x80 : result & ~0x80;

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // C is value of old bit 0
        if ((originalByte & 0b1) != 0)
            registers.setFlagCarry();

        return result;
    }

    int rrN(int originalByte) {
        // Shift byte to the right 1 and make bit 7 value of carry flag
        int result = (originalByte >> 1) & 0xFF;
        result = registers.flagCarry() ? result | 0x80 : result & ~0x80;

        // Reset all flags
        registers.resetAllFlags();
        // Carry flag contains original bit 0 data
        if ((originalByte & 0b1) != 0)
            registers.setFlagCarry();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();

        return result;
    }

    int slaN(int originalByte) {
        int result = (originalByte << 1) & 0xFF;

        // Reset all flags
        registers.resetAllFlags();
        // Carry flag is value of bit 7 from original byte
        if ((originalByte & 0x80) > 0)
            registers.setFlagCarry();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();

        return result;
    }

    int sraN(int originalByte) {
        // Shift byte right 1, keeping most significant bit the same
        int result = ( originalByte >> 1) & 0xFF;
        result = ( originalByte & 0x80) != 0 ? result | 0x80 : result & ~0x80;

        // Reset all flags
        registers.resetAllFlags();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();
        // Set C to value of old 0 bit
        if ((originalByte & 0b1) != 0)
            registers.setFlagCarry();

        return result;
    }

    int srlN(int originalByte) {
        int result = (originalByte >> 1) & 0xFF;

        // Reset all flags
        registers.resetAllFlags();
        // Carry flag is value of bit 0 from original byte
        if ((originalByte & 0x01) > 0)
            registers.setFlagCarry();
        // Set Z if result is zero
        if (result == 0)
            registers.setFlagZero();

        return result;
    }

    void bitBR(int value, int bit) {
        int mask = 0b1 << bit;

        // Set Z if specified bit is 0, else reset
        if ((value & mask) == 0)
            registers.setFlagZero();
        else
            registers.resetFlagZero();
        // Reset N
        registers.resetFlagSubtract();
        // Set H
        registers.setFlagHalfCarry();
    }

    void callNn(int nn) {
        // Put PC for next opcode onto stack
        registers.PC += 2;
        pushTwoBytesToStack(registers.PC);

        // Move PC to new address nn
        registers.PC = nn;
    }

    void ret() {
        // Get the address to return to off the stack
        int returnAddress = popTwoBytesFromStack();

        // Set the PC to that address
        registers.PC = returnAddress;
    }
}
