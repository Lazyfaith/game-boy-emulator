package gameboyemulator.virtualmachine;

import javax.swing.*;
import java.awt.*;

public class Screen extends JPanel {

    public class Sprite {
        // Sprites are made up of 4 bytes
        // Byte 0 - Y coordinate on screen,
        int xPos;
        // Byte 1 - X coordinate on screen
        int yPos;
        // Byte 2 - Pattern/tile to use for sprite
        int patternNum;
        // Byte 3 - Flags affecting sprite behaviour
        // Bit 7
        boolean priority; // 0/False = display on top of BG and Window, 1/True = hidden behind colours 1,2 and 3 of BG and Window
        // Bit 6
        boolean yFlip; // 1/True = flip sprite on vertical axis
        // Bit 5
        boolean xFlip; // 1/True = flip sprite on horizontal axis
        // Bit 4
        boolean paletteNumber; // 1/True = colours from OBJ1PAL, 0/False = 0BJ0PAL
    }

    public static final int SCREENWIDTH = 160, SCREENHEIGHT = 144; // This is the resolution of the physical LCD screen
    public static int SCALAR = 3;

    public static final int GPUMODE_HBLANK = 0b00000000;
    public static final int GPUMODE_VBLANK = 0b00000001;
    public static final int GPUMODE_OAM = 0b00000010;
    public static final int GPUMODE_VRAM = 0b00000011;

    int[][] screenPixels = new int[SCREENWIDTH][SCREENHEIGHT];

    private GameBoyCore gbCore;

    // Flags for which real colour is assigned to the 4 possible shades the Game Boy can display
    public static Color SCREENCOLOURLIGHTEST = Color.WHITE;     // Colour 0
    public static Color SCREENCOLOURLIGHT = Color.LIGHT_GRAY;   // Colour 1
    public static Color SCREENCOLOURDARK = Color.DARK_GRAY;     // Colour 2
    public static Color SCREENCOLOURDARKEST = Color.BLACK;      // Colour 3

    // byteWrittens (some processed already) from RAM to be used more easily when needed

    // Holds cache of tiles with their colouring worked out already so as to save time when actually drawing tiles
    // The colours used here are 0,1,2 and 3. These should then be passed through the BGP register when drawing tiles to see what shades they'll map to
    // NOTE: These are tiles from the tile data table, not how the tiles are ordered & displayed on the screen
    public int[][][] tilesCache = new int[384][8][8]; // [tileNumber][y][x] = colour of pixel x,y in tile tileNumber

    // Holds cache of 40 sprites that can be displayed, already processed for their byteWrittens
    public Sprite[] spritesCache = new Sprite[40]; // [sprite number]

    // Taken from BGP register - Holds palette data used by BG and window
    int[] bgpColours = {0, 3, 3 , 3};

    // LCDC - byteWrittens that control display and what's drawn
    int LCDC = 0x91;

    // SCX, SCY - position of BG map in comparison to real LCD screen & how it should be drawn
    int SCX = 0x00;
    int SCY = 0x00;

    // LY - current scanline
    int LY = 0x00;

    // WX, WY - position of Window for drawing
    int WX = 0x00;
    int WY = 0x00;

    public Screen(GameBoyCore gbCoreInstance) {
        // Reference to the GameBoyCore instance so we can access memory and registers
        gbCore = gbCoreInstance;

        initialiseSpriteCache();

        this.setPreferredSize(new Dimension(SCREENWIDTH * SCALAR, SCREENHEIGHT * SCALAR));
        this.repaint(1);
    }

    public void resetGpu() {
        setGpuMode(GPUMODE_VBLANK);
        cpuCycleCountLastGpuUpdate = 0;
        gpuTicks = 0;

        initialiseSpriteCache();

        clearScreen();
        this.repaint();
    }

    public void initialiseSpriteCache() {
        for(int s=0; s<40; s++)
            spritesCache[s] = new Sprite();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int x=0; x < SCREENWIDTH; x++) {
            for (int y=0; y < SCREENHEIGHT; y++) {
                switch(screenPixels[x][y]) {
                    case 0: g.setColor(SCREENCOLOURLIGHTEST); break;
                    case 1: g.setColor(SCREENCOLOURLIGHT); break;
                    case 2: g.setColor(SCREENCOLOURDARK); break;
                    case 3: g.setColor(SCREENCOLOURDARKEST); break;
                }

                g.fillRect(x * SCALAR, y * SCALAR, SCALAR, SCALAR);
            }
        }

        // Draws  a 1 pixel wide line along the very edges of the display area
        // Useful for checking that none of the Game Boy display is being cut off by window elements
        //g.setColor(Color.RED);
        //g.drawRect(0, 0, (SCREENWIDTH * SCALAR) - 1, (SCREENHEIGHT * SCALAR) - 1);

        g.dispose();
    }

    public void clearScreen() {
        for (int x=0; x < SCREENWIDTH; x++) {
            for (int y = 0; y < SCREENHEIGHT; y++) {
                screenPixels[x][y] = 0;
            }
        }
    }

    public void updateTile(int address) {
        // Using memory address, get tile location in the tilesCache array
        int tile = (address - 0x8000) / 16; // Since first tile is as memory address 0x8000 and each tile is 16 bytes

        // Then get line of pixels in tile being updated
        int y = (address & 0x000F) / 2; // Since there are 16 bytes for each tile, but 2 bytes for each line

        // With this information we can more easily get the address in memory that the tile row starts at
        int tileRowStartAddress = 0x8000 + (tile * 16) + (y * 2);

        // Now we have the tile and the Y byteWritten (row of pixels for that tile)
        //  go through each pixel in that row and update it with the bits from the new byte
        int bitMask;
        for (int x=0; x<8; x++) {
            bitMask = 1 << (7-x);
            int tileColour = (gbCore.ram.readByte(tileRowStartAddress) & bitMask) != 0 ? 1 : 0;
            tileColour += (gbCore.ram.readByte(tileRowStartAddress+1) & bitMask) != 0 ? 2 : 0;
            tilesCache[tile][y][x] = tileColour;
        }
    }

    public void updateSprite(int address, int byteWritten) {
        // The byte has already been cut down to 8 bits

        // First, work out which sprite this relates to
        int spriteNum = (address - 0xFE00) / 4;
        // Then work out which byte of that sprite this is
        int byteNum = (address - 0xFE00) % 4;

        // Then based on this information, update the Sprite accordingly
        switch(byteNum) {
            case 0:
                spritesCache[spriteNum].yPos = byteWritten - 16;
                break;
            case 1:
                spritesCache[spriteNum].xPos = byteWritten - 8;
                break;
            case 2:
                spritesCache[spriteNum].patternNum = byteWritten;
                break;
            case 3:
                spritesCache[spriteNum].priority = (byteWritten & 0b10000000) != 0;
                spritesCache[spriteNum].yFlip = (byteWritten & 0b01000000) != 0;
                spritesCache[spriteNum].xFlip = (byteWritten & 0b00100000) != 0;
                spritesCache[spriteNum].paletteNumber = (byteWritten & 0b00010000) != 0;
                break;
        }
    }

    long cpuCycleCountLastGpuUpdate = 0;
    long gpuTicks = 0;
    public void gpuStep(long cpuCycleCount) {
        // Tick the GPU
        long cyclesToDo = cpuCycleCount - cpuCycleCountLastGpuUpdate;
        cpuCycleCountLastGpuUpdate = cpuCycleCount;
        gpuTicks += cyclesToDo;

        // Based on gpuTicks and current GPU mode, decide what to do (if anything)
        int gpuMode = gbCore.ram.readByte(Memory.AddressSTAT) & 0b00000011; // GPU mode flag is bits 0 and 1

        switch (gpuMode) {
            case GPUMODE_OAM:
                // Reading from OAM
                if (gpuTicks >= 80) {
                    // Just move into next mode
                    gpuTicks -= 80;
                    setGpuMode(GPUMODE_VRAM);
                }
                break;

            case GPUMODE_VRAM:
                // Reading both OAM and VRAM
                if (gpuTicks >= 172) {
                    // Move into H-Blank mode
                    gpuTicks -= 172;
                    setGpuMode(GPUMODE_HBLANK);

                    // Draw the current scanline, as defined by LY register
                    drawScanLine();
                }
                break;

            case GPUMODE_HBLANK:
                // H-Blank
                if (gpuTicks >= 204) {
                    gpuTicks -= 204;

                    // Move to next scanline
                    int newScanline = incrementLY();

                    // See if at last line and time to go into V-Blank mode or if time to start process again for new line
                    if (newScanline == 143) {
                        setGpuMode(GPUMODE_VBLANK);
                        gbCore.ram.requestInterrupt(Memory.InterruptVBlank);

                        // With all scanlines gone through, update the screen with latest graphical data
                        updateScreen();
                    }
                    else
                        setGpuMode(GPUMODE_OAM);
                }
                break;

            case GPUMODE_VBLANK:
                // V-Blank
                if (gpuTicks >= 456) {
                    gpuTicks -= 456;
                    int newScanLine = incrementLY();

                    // We spend 10 scanlines worth of time in V-Blank before going back to first scanline
                    if (newScanLine > 153) {
                        // Reset scanline to 0 and start whole process again
                        gbCore.ram.forceByte(Memory.AddressLY, 0x00);
                        setGpuMode(GPUMODE_OAM);
                    }
                }
                break;
        }
    }

    void setGpuMode(int mode) {
        int newStatbyteWritten = gbCore.ram.combineWithCurrentVal(Memory.AddressSTAT, mode, 0b00000011);
        if (GameBoyCore.VERBOSEDEBUGLOG)
            System.out.println("Updating STAT byteWritten to GPU mode " + mode + ":");
        gbCore.ram.forceByte(Memory.AddressSTAT, newStatbyteWritten);
    }

    int incrementLY() {
        // Moves GPU to next scanline and returns the number of the new scanline
        int newScanline = gbCore.ram.readByte(Memory.AddressLY) + 1;
        if (GameBoyCore.VERBOSEDEBUGLOG)
            System.out.println("Updating LY byteWritten to " + newScanline + ":");
        gbCore.ram.forceByte(Memory.AddressLY, newScanline);
        return newScanline;
    }

    int[] processPalette(int paletteData) {
        // Reads palette data from a byte and returns an array with the 4 colours worked out
        int[] data = new int[4];
        data[0] = paletteData & 0b00000011;
        data[1] = (paletteData & 0b00001100) >> 2;
        data[2] = (paletteData & 0b00110000) >> 4;
        data[3] = (paletteData & 0b11000000) >> 6;
        return data;
    }

    void drawScanLine() {
        // Common variables that are needed by more than just 1 layer of drawing
        int LY = gbCore.ram.readByte(Memory.AddressLY);
        int LCDC = gbCore.ram.readByte(Memory.AddressLCDC);
        int[] OBP0 = processPalette(gbCore.ram.readByte(Memory.AddressOBP0));
        int[] OBP1 = processPalette(gbCore.ram.readByte(Memory.AddressOBP1));

        // Draw background
        if ((LCDC & 0b1) != 0) {
            // BG is enabled!
            // Get the background related variables we'll need
            int SCX = gbCore.ram.readByte(Memory.AddressSCX);
            int SCY = gbCore.ram.readByte(Memory.AddressSCY);

            // Get which tilemap we'll be going through by consulting LCDC
            int mapStartAddress = (LCDC & 0b1000) == 0 ? 0x9800 : 0x9C00;

            // Then find line of tiles we need to be reading from
            int lineOfTiles = mapStartAddress + ((((LY + SCY) & 0xFF) >>3)<<5);

            // Find which tile in that line we start with
            int lineOffset = SCX % 32;

            // Then work out exactly which pixels we start from within that tile
            int xInTile = SCX % 8;
            int yInTile = (LY + SCY) % 8;

            // Get the palette data from the appropriate palette
            int[] palette = processPalette(gbCore.ram.readByte(Memory.AddressBGP));

            // Now can iterate through tiles & pixels until we've drawn along the whole scanline
            int tile = gbCore.ram.readByte(lineOfTiles + lineOffset);

            for(int screenX=0; screenX<SCREENWIDTH; screenX++) {
                // Pass pixel through palette to get right colour before putting onto screen
                screenPixels[screenX][LY] = palette[tilesCache[tile][yInTile][xInTile]];

                xInTile++;
                if (xInTile == 8) {
                    xInTile = 0;
                    lineOffset = (lineOffset + 1) & 31; // Since if we go off the edge of the background ( > 31 ) we just wrap around it
                    tile = gbCore.ram.readByte(lineOfTiles + lineOffset);
                }
            }
        }
        else {
            // BG is disabled! Just fill it in with white
            for (int x=0; x<SCREENWIDTH; x++)
                screenPixels[x][LY] = 0;
        }

        // Draw sprites
        if ((LCDC & 0b10) != 0) {
            // Sprites are enabled!

            // Go through every sprite in the cache
            for (int s=0; s<40; s++) {
                // Flip the number so we start at the last sprite and then go through them all to the first one
                // This is so the sprites are drawn over any sprite that comes after it in the sprite map
                Sprite sprite = spritesCache[39 - s];

                // Check if sprite shows up on the current scanline
                if(sprite.yPos <= LY && (sprite.yPos+8) > LY) {
                    // Get the sprite palette
                    int[] spritePalette = sprite.paletteNumber ? OBP1 : OBP0;
                    
                    // Work out which pixel row from the tile we're reading
                    int tileRow = LY - sprite.yPos;
                    // If sprite is flipped on Y axis then need to adjust row number
                    if (sprite.yFlip)
                        tileRow = 7 - tileRow;
                    
                    // Now go through each pixel in the tile row
                    for(int x=0; x<8; x++) {
                        // Check if pixel is on screen
                        int screenX = sprite.xPos + x;
                        if ((screenX >= 0) && (screenX < 160)) {
                            // Check if sprite has priority to show over background (or background at this pixel is colour 0)
                            if (sprite.priority || screenPixels[screenX][LY] == 0) {
                                // We're drawing this pixel!
                                // Check if flipping tile on X axis
                                int tileXPixel = sprite.xFlip ? (7 - x) : x;
                                // Then pass tile through appropriate palette before putting on screen
                                screenPixels[screenX][LY] = spritePalette[tilesCache[sprite.patternNum][tileRow][tileXPixel]];
                            }
                        }
                    }
                }
            }
        }
    }

    void updateScreen() {
        // Dump as many tiles from VRAM to screen as can fit (for testing)
//        for(int screenY=0; screenY<18; screenY++) {
//            for(int screenX=0; screenX<20; screenX++) {
//                for (int pixelY = 0; pixelY < 8; pixelY++) {
//                    for (int pixelX = 0; pixelX < 8; pixelX++) {
//                        screenPixels[pixelX + (screenX * 8)][pixelY + (screenY * 8)] = tilesCache[(screenY*20) + screenX][pixelY][pixelX];
//                    }
//                }
//            }
//        }

        this.repaint();
    }
}